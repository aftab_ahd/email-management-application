package jarir.ems.dto;

import com.jarir.ems.entities.ems.JarirEmail;

public class TicketLinkUpdateDto {
	private long ticketId;
	private JarirEmail email;

	public long getTicketId() {
		return ticketId;
	}

	public void setTicketId(long ticketId) {
		this.ticketId = ticketId;
	}

	public JarirEmail getEmail() {
		return email;
	}

	public void setEmail(JarirEmail email) {
		this.email = email;
	}

}
