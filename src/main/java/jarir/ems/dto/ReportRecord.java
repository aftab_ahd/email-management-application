package jarir.ems.dto;

import java.util.Date;

public class ReportRecord {
	private Date date;

	private int received;

	private int offers;
	private int crm;
	private int noaction;
	private int duplicate;
	private int inquiry;
	private int books;
	private int installment;
	private int hr;
	private int ebusiness;
	private int discountCard;
	private int corporateSales;
	private int afterSales;
	private int internal;
	private int sponsoringcharity;
	private int other;

	public ReportRecord(Date date, int received, int offers, int crm, int noaction, int duplicate, int inquiry,
			int books, int installment, int hr, int ebusiness, int discountCard, int corporateSales, int afterSales,
			int internal, int sponsoringcharity, int other) {
		super();
		this.date = date;
		this.received = received;
		this.offers = offers;
		this.crm = crm;
		this.noaction = noaction;
		this.duplicate = duplicate;
		this.inquiry = inquiry;
		this.books = books;
		this.installment = installment;
		this.hr = hr;
		this.ebusiness = ebusiness;
		this.discountCard = discountCard;
		this.corporateSales = corporateSales;
		this.afterSales = afterSales;
		this.internal = internal;
		this.sponsoringcharity = sponsoringcharity;
		this.other = other;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public int getReceived() {
		return received;
	}

	public void setReceived(int received) {
		this.received = received;
	}

	public int getOffers() {
		return offers;
	}

	public void setOffers(int offers) {
		this.offers = offers;
	}

	public int getCrm() {
		return crm;
	}

	public void setCrm(int crm) {
		this.crm = crm;
	}

	public int getNoaction() {
		return noaction;
	}

	public void setNoaction(int noaction) {
		this.noaction = noaction;
	}

	public int getDuplicate() {
		return duplicate;
	}

	public void setDuplicate(int duplicate) {
		this.duplicate = duplicate;
	}

	public int getInquiry() {
		return inquiry;
	}

	public void setInquiry(int inquiry) {
		this.inquiry = inquiry;
	}

	public int getBooks() {
		return books;
	}

	public void setBooks(int books) {
		this.books = books;
	}

	public int getInstallment() {
		return installment;
	}

	public void setInstallment(int installment) {
		this.installment = installment;
	}

	public int getHr() {
		return hr;
	}

	public void setHr(int hr) {
		this.hr = hr;
	}

	public int getEbusiness() {
		return ebusiness;
	}

	public void setEbusiness(int ebusiness) {
		this.ebusiness = ebusiness;
	}

	public int getDiscountCard() {
		return discountCard;
	}

	public void setDiscountCard(int discountCard) {
		this.discountCard = discountCard;
	}

	public int getCorporateSales() {
		return corporateSales;
	}

	public void setCorporateSales(int corporateSales) {
		this.corporateSales = corporateSales;
	}

	public int getAfterSales() {
		return afterSales;
	}

	public void setAfterSales(int afterSales) {
		this.afterSales = afterSales;
	}

	public int getInternal() {
		return internal;
	}

	public void setInternal(int internal) {
		this.internal = internal;
	}

	public int getSponsoringcharity() {
		return sponsoringcharity;
	}

	public void setSponsoringcharity(int sponsoringcharity) {
		this.sponsoringcharity = sponsoringcharity;
	}

	public int getOther() {
		return other;
	}

	public void setOther(int other) {
		this.other = other;
	}

}
