package jarir.ems.dto;

public class PieData {
	String label;
	int count;
	public PieData(String label, int count) {
		super();
		this.label = label;
		this.count = count;
	}
}
