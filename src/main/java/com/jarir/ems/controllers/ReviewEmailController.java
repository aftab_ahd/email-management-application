package com.jarir.ems.controllers;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLConnection;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;

import org.keycloak.KeycloakPrincipal;
import org.keycloak.KeycloakSecurityContext;
import org.keycloak.representations.idm.UserRepresentation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jarir.ems.entities.ems.EmailAction;
import com.jarir.ems.entities.ems.EmailAttachment;
import com.jarir.ems.entities.ems.EmailResolution;
import com.jarir.ems.entities.ems.EmailTemplate;
import com.jarir.ems.entities.ems.EmailTemplateParameter;
import com.jarir.ems.entities.ems.JarirEmail;
import com.jarir.ems.entities.ems.Ticket;
import com.jarir.ems.entities.ems.TicketAction;
import com.jarir.ems.repos.ems.EmailAttachmentRepository;
import com.jarir.ems.repos.ems.EmailResolutionRepository;
import com.jarir.ems.repos.ems.EmailTemplateCategoryRepository;
import com.jarir.ems.repos.ems.EmailTemplateRepository;
import com.jarir.ems.repos.ems.JarirEmailRepository;
import com.jarir.ems.repos.ems.TicketRepository;
import com.jarir.ems.services.JarirEmailService;
import com.jarir.ems.services.UserRetrievalService;
import com.jarir.ems.util.EmailReadingUtils;
import com.jarir.ems.util.ViewHelper;

import jarir.ems.dto.EmailActionDto;
import jarir.ems.dto.TicketLinkUpdateDto;


@Controller
public class ReviewEmailController {

	@Autowired
	JarirEmailRepository emailRepository;
	
	@Autowired
	TicketRepository ticketRepository;
	
	@Autowired
	EmailAttachmentRepository emailAttachmentRepository;
	
	@Autowired
	EmailTemplateRepository emailTemplateRepository;
	
	@Autowired
	EmailTemplateCategoryRepository emailTemplateCategoryRepository;
	
	@Autowired
	EmailResolutionRepository emailResolutionRepository;
	
	@Autowired
	UserRetrievalService userService;
	
	@Autowired
	JarirEmailService emailService;
	
	@Autowired
	ViewHelper viewHelper;
	
	EmailReadingUtils emailReadingUtils = new EmailReadingUtils();
	
	@GetMapping("/reviewemail/{id}")
	public String reviewEmail(@PathVariable("id") long id, Model model) {
		JarirEmail email = emailRepository.findById(id).get();
		model.addAttribute("templates", emailTemplateRepository.findAll());
		model.addAttribute("templatesCategories", emailTemplateCategoryRepository.findAll());
		model.addAttribute("resolutions", emailResolutionRepository.findAll());
		model.addAttribute("email", email);
//		model.addAttribute("tickets", ticketRepository.findAllByStatusNot("Closed"));
		
		model.addAttribute("response", buildResponse(email));
		model = viewHelper.updateCounts(model);
		return "emails/reviewemail";
	}
	
	private String buildResponse(JarirEmail email) {
		String response = "";
		
		if(!email.getStatus().equals("NoAction") && !email.getStatus().equals("Pending")) {
			if(email.getEmailAction().getLanguage().equals("Arabic")) {
				response = emailReadingUtils.includeParameters(email.getEmailAction().getEmailTemplate().getBodyArabic(), email.getEmailAction().getParameters());
		    }else {
		    	response = emailReadingUtils.includeParameters(email.getEmailAction().getEmailTemplate().getBodyEnglish(), email.getEmailAction().getParameters());
		    }
		}
		
		return response;
	}
	
	@GetMapping(value="/autocompleteEmail", produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody UserRepresentation[] autocompleteEmail(@RequestParam String emailToSearch) {
		UserRepresentation[] userEmails = null;
		try {
			userEmails = userService.getUsersByName(URLDecoder.decode(emailToSearch, "UTF-8"));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		
	    return userEmails;
	}

	@RequestMapping(value = "/getImage/{imageId}", method = RequestMethod.GET)
	public ResponseEntity<byte[]> getImage(@PathVariable final String imageId) {
		EmailAttachment attachment = emailAttachmentRepository.findById(Long.parseLong(imageId)).get();
		try {
			File file = null;
			String path = attachment.getFilepath();
			FileInputStream fileStream = new FileInputStream(
					file = new File(path));
			byte[] bytes = new byte[(int) file.length()];
			fileStream.read(bytes, 0, bytes.length);
			final HttpHeaders headers = new HttpHeaders();
			if(URLConnection.guessContentTypeFromName(path)!=null) {
				headers.setContentType(MediaType.valueOf(URLConnection.guessContentTypeFromName(path)));
			}
			fileStream.close();
			return new ResponseEntity<byte[]>(bytes, headers, HttpStatus.CREATED);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return new ResponseEntity<byte[]>(null, null, HttpStatus.NOT_ACCEPTABLE);
	}
	
	private String getCurrentUsername() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		@SuppressWarnings("unchecked")
		KeycloakPrincipal<KeycloakSecurityContext> keycloakPrinciple = (KeycloakPrincipal<KeycloakSecurityContext>) authentication.getPrincipal();
		return keycloakPrinciple.getName();
	}
	
	@PostMapping(value="/emailaction")
	public @ResponseBody String emailAction(@RequestBody final EmailActionDto actionDto, Model model) {
		EmailAction action = new EmailAction();
		EmailTemplate template = null;
		if(actionDto.getTemplateId()!=0) {
			template = emailTemplateRepository.findById(actionDto.getTemplateId()).get();
		}
		EmailResolution resolution = emailResolutionRepository.findById(actionDto.getResolutionId()).get();
		JarirEmail email = emailRepository.findById(actionDto.getEmailId()).get();
		String lang = actionDto.getLang();
		String status = actionDto.getStatus();
		String currentUser = getCurrentUsername();
		List<EmailTemplateParameter> parameters = new ArrayList<>();
		int counter = 1;
		for(String param : actionDto.getParams()) {
			EmailTemplateParameter parameter = new EmailTemplateParameter();
			parameter.setName("\\$"+(counter));
			parameter.setValue(param);
			parameter.setEmailAction(action);
			parameters.add(parameter);
			counter++;
		}
		
		action.setEmailTemplate(template);
		action.setEmailResolution(resolution);
		action.setJarirEmail(email);
		action.setLanguage(lang);
		action.setParameters(parameters);
		action.setForwardTo(actionDto.getForwardTo());
		action.setUserId(currentUser);
		
		email.setEmailAction(action);
		email.setStatus(status);
		
		emailRepository.save(email);
		
		emailService.handleAction(action);
//		emailService.saveAction(action);
		
		return "{\"response\":1}";
		//return new ResponseEntity<>("{\"response\" : 1}", HttpStatus.OK);
	}
	
	@PostMapping("/emailnoaction")
    public ResponseEntity<String> noAction(@RequestBody final EmailActionDto actionDto, Model model) {
		EmailAction action = new EmailAction();
		
		JarirEmail email = emailRepository.findById(actionDto.getEmailId()).get();
		action.setUserId(getCurrentUsername());
		email.setEmailAction(action);
		email.setStatus("NoAction");
		
		emailRepository.save(email);
		emailService.handleNoAction(email);

		return new ResponseEntity<String>("{\"response\" : 1}", HttpStatus.OK);
    }
		
	@PostMapping(value="/linktonewticket")
	public String linkToNewTicket(@ModelAttribute TicketLinkUpdateDto ticketLinkUpdateDto) {
		Ticket ticket = new Ticket();
		JarirEmail email = emailRepository.findById(ticketLinkUpdateDto.getEmail().getId()).get();
		
		TicketAction existingTicketAction = new TicketAction();
		existingTicketAction.setAction("Email Un-Linked to New Ticket");
		existingTicketAction.setUserId(getCurrentUsername());
		existingTicketAction.setTicket(email.getTicket());
		
		email.getTicket().getActions().add(existingTicketAction);
		long initialTicketId = email.getTicket().getId(); // Or save below into Ticket
		ticketRepository.save(email.getTicket());
		
		ticket.setStatus("Pending");
		ticket.getEmails().add(email);
		email.setTicket(ticket);
		
		TicketAction ticketAction = new TicketAction();
		ticketAction.setAction("Email Linked to This New Ticket from Ticket ID:"+initialTicketId);
		ticketAction.setUserId(getCurrentUsername());
		ticketAction.setTicket(ticket);
		
		ticket.getActions().add(ticketAction);
		ticketRepository.save(ticket);
		
		return "redirect:/reviewemail/"+ticketLinkUpdateDto.getEmail().getId();
	}

	@PostMapping(value="/linktoticket")
	public String linkToExistingTicket(@ModelAttribute TicketLinkUpdateDto ticketLinkUpdateDto) {
		Ticket ticket = ticketRepository.findById(ticketLinkUpdateDto.getTicketId()).get();
		JarirEmail email = emailRepository.findById(ticketLinkUpdateDto.getEmail().getId()).get();
		
		if(email.getTicket().getEmails().size() == 1) {
			email.getTicket().setStatus("Closed");
			TicketAction ticketAction = new TicketAction();
    		ticketAction.setAction("Ticket Closed (No More E-Mails)");
    		ticketAction.setUserId(getCurrentUsername());
    		ticketAction.setTicket(email.getTicket());
    		
    		email.getTicket().getActions().add(ticketAction);
    		ticketRepository.save(email.getTicket());
		} else {
		
			TicketAction existingTicketAction = new TicketAction();
			existingTicketAction.setAction("Email Un-Linked to Ticket ID:"+ticket.getId());
			existingTicketAction.setUserId(getCurrentUsername());
			existingTicketAction.setTicket(email.getTicket());
			
			email.getTicket().getActions().add(existingTicketAction);
			ticketRepository.save(email.getTicket());
			
			
			TicketAction linkedTicketAction = new TicketAction();
			linkedTicketAction.setAction("New Email Linked from Ticket ID:"+email.getTicket().getId());
			linkedTicketAction.setUserId(getCurrentUsername());
			linkedTicketAction.setTicket(ticket);
			
			ticket.getEmails().add(email);
			email.setTicket(ticket);
			
			ticket.getActions().add(linkedTicketAction);
			ticketRepository.save(ticket);
		}

		return "redirect:/reviewemail/"+ticketLinkUpdateDto.getEmail().getId();
	}
	
}
