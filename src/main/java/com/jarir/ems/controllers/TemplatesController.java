package com.jarir.ems.controllers;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jarir.ems.entities.ems.EmailTemplate;
import com.jarir.ems.entities.ems.EmailTemplateCategory;
import com.jarir.ems.repos.ems.EmailTemplateCategoryRepository;
import com.jarir.ems.repos.ems.EmailTemplateRepository;
import com.jarir.ems.util.ViewHelper;

@Controller
public class TemplatesController {
	
	@Autowired
	EmailTemplateRepository emailTemplateRepository;
	
	@Autowired
	EmailTemplateCategoryRepository emailTemplateCategoryRepository;
	
	@Autowired
	ViewHelper viewHelper;
	
	@GetMapping(value="/templates")
	public String showTemplatesPage(Model model) {
		model.addAttribute("templates", emailTemplateRepository.findAll());
		model = viewHelper.updateCounts(model);
		return "templates";
	}
	
	@GetMapping(value="/newtemplatepage")
	public String newTemplatePage(EmailTemplate emailTemplate, Model model) {
		model.addAttribute("templateCategories", emailTemplateCategoryRepository.findAll());
		model = viewHelper.updateCounts(model);
		return "emailtemplates/add";
	}
	
	@PostMapping(value="addtemplate")
	public String addTemplate(@Valid EmailTemplate emailTemplate, BindingResult errors, Model model) {
		
		if (errors.hasErrors()) {
			model = viewHelper.updateCounts(model);
            return "emailtemplates/add";
        }

		emailTemplate.setParametersCountAra(countParamters(emailTemplate.getBodyArabic()));
		emailTemplate.setParametersCountEng(countParamters(emailTemplate.getBodyEnglish()));
		emailTemplateRepository.save(emailTemplate);

		return "redirect:/templates";
	}
	
	private int countParamters(String body) {
		int count = 0;
		Pattern pattern = Pattern.compile("\\$[0-9]*");
		Matcher matcher = pattern.matcher(body);
		while (matcher.find())
		    count++;
		return count;
	}
	
	@GetMapping("/edittemplate/{id}")
    public String showUpdateForm(@PathVariable("id") long id, Model model) {		
		EmailTemplate emailTemplate = emailTemplateRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid template Id:" + id));
        model.addAttribute("emailTemplate", emailTemplate);
        model.addAttribute("templateCategories", emailTemplateCategoryRepository.findAll());
        model = viewHelper.updateCounts(model);
        return "emailtemplates/edit";
    }
    
    @PostMapping("/updatetemplate/{id}")
    public String updateTemplate(@PathVariable("id") long id, @Valid EmailTemplate emailTemplate, BindingResult result, Model model) {
        if (result.hasErrors()) {
        	emailTemplate.setId(id);
        	model = viewHelper.updateCounts(model);
            return "emailtemplates/edit";
        }
        emailTemplate.setParametersCountAra(countParamters(emailTemplate.getBodyArabic()));
		emailTemplate.setParametersCountEng(countParamters(emailTemplate.getBodyEnglish()));
        emailTemplateRepository.save(emailTemplate);

        return "redirect:/templates";
    }
    
    @GetMapping("/deletetemplate/{id}")
    public String deleteTemplate(@PathVariable("id") long id, Model model) {
    	EmailTemplate emailTemplate = emailTemplateRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid template Id:" + id));
        emailTemplateRepository.delete(emailTemplate);
        return "redirect:/templates";
    }
    
//    @GetMapping(value = "/gettemplates/{templateCategoryId}")
//    public @ResponseBody List<EmailTemplate> getTemplates(@PathVariable("templateCategoryId") Long templateCategoryId) {
//    	EmailTemplateCategory emailTemplateCategory = emailTemplateCategoryRepository.findById(templateCategoryId).orElseThrow(() -> new IllegalArgumentException("Invalid templateCategoryId Id:" + templateCategoryId));
//    	return emailTemplateRepository.findByEmailTemplateCategory(emailTemplateCategory);
//    }
    
    @GetMapping(value = "/gettemplates")
    public @ResponseBody List<EmailTemplate> findAllAgencies(@RequestParam(value = "templateCategoryId", required = true) Long templateCategoryId) {
    	EmailTemplateCategory emailTemplateCategory = emailTemplateCategoryRepository.findById(templateCategoryId).orElseThrow(() -> new IllegalArgumentException("Invalid templateCategoryId Id:" + templateCategoryId));
    	return emailTemplateRepository.findByEmailTemplateCategory(emailTemplateCategory);
    }

}
