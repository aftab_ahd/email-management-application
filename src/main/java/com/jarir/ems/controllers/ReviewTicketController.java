package com.jarir.ems.controllers;

import javax.validation.Valid;

import org.keycloak.KeycloakPrincipal;
import org.keycloak.KeycloakSecurityContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import com.jarir.ems.entities.ems.Comment;
import com.jarir.ems.entities.ems.Ticket;
import com.jarir.ems.entities.ems.TicketAction;
import com.jarir.ems.repos.ems.CommentRepository;
import com.jarir.ems.repos.ems.TicketRepository;
import com.jarir.ems.util.ViewHelper;

@Controller
public class ReviewTicketController {
	
	@Autowired
	TicketRepository ticketRepository;
	
	@Autowired
	CommentRepository commentRepository;
	
	@Autowired
	ViewHelper viewHelper;
	
	@GetMapping("/reviewticket/{id}")
	public String reviewTicket(Comment comment, @PathVariable("id") long id, Model model) {
		Ticket ticket = ticketRepository.findById(id).get();
		model.addAttribute("ticket", ticket);
		model = viewHelper.updateCounts(model);
//		return "tickets/reviewTicket";
		return "redirect:/showNewEmails";
	}
	
	private String getCurrentUsername() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		@SuppressWarnings("unchecked")
		KeycloakPrincipal<KeycloakSecurityContext> keycloakPrinciple = (KeycloakPrincipal<KeycloakSecurityContext>) authentication.getPrincipal();
		return keycloakPrinciple.getName();
	}
	
	@GetMapping("/markticketclosed/{id}")
    public String markClosed(@PathVariable("id") long id, Model model) {
		Ticket ticket = ticketRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid ticket Id:" + id));
		boolean cannotClose = ticket.getEmails().stream().anyMatch(t -> t.getStatus().equals("Pending"));
		if(cannotClose) {
			model.addAttribute("cannotClose", "Cannot Close Ticket with Pending E-mails");
			model.addAttribute("ticket", ticket);
			model.addAttribute("comment", new Comment());
			model = viewHelper.updateCounts(model);
			return "tickets/reviewTicket";
		} else {
			ticket.setStatus("Closed");
    		
    		TicketAction ticketAction = new TicketAction();
    		ticketAction.setAction("Ticket Closed");
    		ticketAction.setUserId(getCurrentUsername());
    		ticketAction.setTicket(ticket);
    		
    		ticket.getActions().add(ticketAction);
    		ticketRepository.save(ticket);
		}
    	
        return "redirect:/pendingtickets";
    }
	
	@PostMapping("/addcomment/{ticketId}")
	public String addComment(@PathVariable("ticketId") long ticketId, @Valid Comment comment, BindingResult errors, Model model) {

		Ticket ticket = ticketRepository.findById(ticketId).get();
		if (errors.hasErrors()) {
			model.addAttribute("ticket", ticket);
//			model.addAttribute("comment", new Comment());
			model = viewHelper.updateCounts(model);
			return "tickets/reviewTicket";
        }

		comment.setUserId(getCurrentUsername());
		comment.setTicket(ticket);
		commentRepository.save(comment);
		
		TicketAction ticketAction = new TicketAction();
		ticketAction.setAction("Comment Added");
		ticketAction.setUserId(getCurrentUsername());
		ticketAction.setTicket(ticket);
		
		ticket.getActions().add(ticketAction);
		ticketRepository.save(ticket);
		
		model.addAttribute("ticket", ticket);
		model.addAttribute("comment", new Comment());
		model = viewHelper.updateCounts(model);
		return "tickets/reviewTicket";
	}
}
