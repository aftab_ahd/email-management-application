package com.jarir.ems.controllers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import com.jarir.ems.entities.ems.EmailAccount;
import com.jarir.ems.repos.ems.EmailAccountRepository;
import com.jarir.ems.util.ViewHelper;

@Controller
public class AccountsController {
	
	@Autowired
	EmailAccountRepository emailAccountRepository;
	
	@Autowired
	ViewHelper viewHelper;
	
	@GetMapping(value="/accounts")
	public String showAccountsPage(Model model) {
		model.addAttribute("accounts", emailAccountRepository.findAll());
		model = viewHelper.updateCounts(model);
		return "accounts";
	}
	
	@GetMapping(value="/newaccountpage")
	public String newAccountPage(EmailAccount emailAccount, Model model) {
		model = viewHelper.updateCounts(model);
		return "emailaccounts/add";
	}
	
	@PostMapping(value="addaccount")
	public String addAccount(@Valid EmailAccount emailAccount, BindingResult errors, Model model) {
		if (errors.hasErrors()) {
			model = viewHelper.updateCounts(model);
            return "emailaccounts/add";
        }
         
		emailAccountRepository.save(emailAccount);

		return "redirect:/accounts";
	}
	
	@GetMapping("/editaccount/{id}")
    public String showUpdateForm(@PathVariable("id") long id, Model model) {
		EmailAccount emailAccount = emailAccountRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid account Id:" + id));
        model.addAttribute("emailAccount", emailAccount);
        model = viewHelper.updateCounts(model);
        return "emailaccounts/edit";
    }
    
    @PostMapping("/updateaccount/{id}")
    public String updateAccount(@PathVariable("id") long id, @Valid EmailAccount emailAccount, BindingResult result, Model model) {
        if (result.hasErrors()) {
        	emailAccount.setId(id);
        	model = viewHelper.updateCounts(model);
            return "emailaccounts/edit";
        }
        
        emailAccountRepository.save(emailAccount);

        return "redirect:/accounts";
    }
    
    @GetMapping("/deleteaccount/{id}")
    public String deleteAccount(@PathVariable("id") long id, Model model) {
    	EmailAccount emailAccount = emailAccountRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid account Id:" + id));
        emailAccountRepository.delete(emailAccount);
        return "redirect:/accounts";
    }

}
