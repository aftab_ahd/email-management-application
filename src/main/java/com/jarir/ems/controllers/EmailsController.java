package com.jarir.ems.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import com.jarir.ems.entities.ems.JarirEmail;
import com.jarir.ems.repos.ems.EmailResolutionRepository;
import com.jarir.ems.repos.ems.EmailTemplateCategoryRepository;
import com.jarir.ems.repos.ems.EmailTemplateRepository;
import com.jarir.ems.repos.ems.JarirEmailRepository;
import com.jarir.ems.util.ViewHelper;


@Controller
public class EmailsController {

	@Autowired
	JarirEmailRepository emailRepository;
	
	@Autowired
	ViewHelper viewHelper;
	
//	@Autowired
//	JarirEmailService emailService;
	
	@Autowired
	EmailTemplateRepository emailTemplateRepository;
	
	@Autowired
	EmailTemplateCategoryRepository emailTemplateCategoryRepository;
	
	@Autowired
	EmailResolutionRepository emailResolutionRepository;
	
	@GetMapping("/showNoactionEmails")
	public String showNoactionEmails(Model model) {
		List<JarirEmail> noactionEmails = emailRepository.findByStatus("NoAction");
		model.addAttribute("emails", noactionEmails);
		model = viewHelper.updateCounts(model);
		return "emails/noactionEmails";
	}
	
	@GetMapping("/showFollowupEmails")
	public String showFollowupEmails(Model model) {
		List<JarirEmail> followupEmails = emailRepository.findByStatus("FollowUp");
		model.addAttribute("emails", followupEmails);
		model = viewHelper.updateCounts(model);
		return "emails/followupEmails";
	}
	
	@GetMapping("/showClosedEmails")
	public String showClosedEmails(Model model) {
		List<JarirEmail> repliedEmails = emailRepository.findByStatus("Replied");
		List<JarirEmail> forwardedEmails = emailRepository.findByStatus("Replied and Forwarded");
		List<JarirEmail> closedEmails = new ArrayList<>();
		
		closedEmails.addAll(repliedEmails);
		closedEmails.addAll(forwardedEmails);
		
		model.addAttribute("emails", closedEmails);
		model = viewHelper.updateCounts(model);
		return "emails/closedEmails";
	}
	
	@GetMapping("/showNewEmails")
	public String showNewEmails(Model model) {
		List<JarirEmail> newEmails = emailRepository.findByStatus("Pending");
		model.addAttribute("emails", newEmails);
		model.addAttribute("templates", emailTemplateRepository.findAll());
		model.addAttribute("templatesCategories", emailTemplateCategoryRepository.findAll());
		model.addAttribute("resolutions", emailResolutionRepository.findAll());
		model = viewHelper.updateCounts(model);
//		return "emails/newEmails";
		return "emails/pendingEmails";
	}
	
//	@SuppressWarnings("unused")
//	private boolean isEmailAlreadyIncluded(JarirEmail email, List<JarirEmail> includedEmails) {
//		for(JarirEmail incluEmail : includedEmails) {
//			if(emailService.isMessageIdEquals(incluEmail.getMessageId(), email.getMessageId())) {
//				return true;
//			}
//			for(JarirEmail referencedEmail : incluEmail.getReferencedEmails()){
//				if(emailService.isMessageIdEquals(referencedEmail.getMessageId(), email.getMessageId())) {
//					return true;
//				}
//			}
//		}
//		return false;
//	}
//	
//	private List<JarirEmail> sortAndUpdateReferencedEmails(List<JarirEmail> emails) {
//		List<JarirEmail> finalEmails = new ArrayList<>();
//		// Newest First
//		//   Oldest First (o1, o2) -> o1.getSentDate().compareTo(o2.getSentDate())
//		emails.sort((o1, o2) -> o2.getSentDate().compareTo(o1.getSentDate()));
//		for(JarirEmail email : emails) {
//			//if(!isEmailAlreadyIncluded(email, finalEmails)) {
//				finalEmails.add(findReferencedEmails(email));
//			//}
//		}
//		return finalEmails;
//	}
//	private boolean isMessageIdEquals(String messageId1, String messageId2) {
//	if(messageId1.matches(pattern) && messageId2.matches(pattern)) {
//		if(messageId1.substring(1, 20).equals(messageId2.substring(1, 20))) {
//			return true;
//		}
//	} else if(messageId1.equals(messageId2)) {
//		return true;
//	}
//	return false;
//}
//	@GetMapping("/reviewemail/{id}")
//	public String reviewEmail(@PathVariable("id") long id, Model model) {
//		JarirEmail email = emailRepository.findById(id).get();
//		List<JarirEmail> referencedEmails = findReferencedEmails(email);
//		
//		//Newest First
//		referencedEmails.sort((o1, o2) -> o2.getSentDate().compareTo(o1.getSentDate()));
//		email.setReferencedEmails(referencedEmails);
//		
//		String response = "";
//		
//		if(!email.getStatus().equals("NoAction")) {
//			if(email.getEmailAction().getLanguage().equals("Arabic")) {
//				response = emailUtils.includeParameters(email.getEmailAction().getEmailTemplate().getBodyArabic(), email.getEmailAction().getParameters());
//		    }else {
//		    	response += emailUtils.includeParameters(email.getEmailAction().getEmailTemplate().getBodyEnglish(), email.getEmailAction().getParameters());
//		    }
//		}
//
//		model.addAttribute("email", email);
//		model.addAttribute("response", response);
//		model = viewHelper.updateCounts(model);
//		return "emails/reviewemail";
//	}
//	private List<JarirEmail> findReferencedEmails(JarirEmail email) {
//	
////	System.out.println("For Email with subject " + email.getSubject());
//	//For the email message-id
//	//  Find in other emails if it is in References 
//	
//	//Below is wrong
////	String messageId = email.getMessacgeId();
////	String inReplyTo = email.getInReplyTo();
//	List<JarirEmail> referencedEmails = new ArrayList<>();
//	if(!email.getReferencesEmails().isEmpty()) {
//		String[] references = email.getReferencesEmails().split(" ");
////		System.out.println("init references = " + references.length);
//		for(int i = 0 ; i < references.length; i++) {
//			references[i] = references[i].replace("\n", "").replace("\r", "");
//			for(JarirEmail emailToCheck : emailRepository.findAll()) {
//				String messageIdToCheck = emailToCheck.getMessageId();
////				System.out.println("Comparing:");
////				System.out.println("'"+references[i]+"'");
////				System.out.println("To:");
////				System.out.println("'"+messageIdToCheck+"'");
//				if(isMessageIdEquals(references[i], messageIdToCheck)) {
//					referencedEmails.add(emailToCheck);
//				}
//			}
//		}
//	}
//	return referencedEmails;
//}
//	private JarirEmail findRepliedToEmail(JarirEmail email) {
//	
//	JarirEmail repliedToEmails = null;
//	String inReplyTo = email.getInReplyTo();
//	if(inReplyTo!=null && !inReplyTo.isEmpty()) {
//		for(JarirEmail emailToCheck : emailRepository.findAll()) {
//			String messageIdToCheck = emailToCheck.getMessageId();
//			if(isMessageIdEquals(inReplyTo, messageIdToCheck)) {
//				System.out.println("FOUND " + inReplyTo +" = "+ messageIdToCheck);
//				repliedToEmails = emailToCheck;
//			}
//		}
//	}
//	
//	return repliedToEmails;
//}
//	EmailReadingUtils emailUtils = new EmailReadingUtils();
//	private String pattern = "<([A-Z]|[0-9]){10}\\.([A-Z]|[0-9]){8}-([A-Z]|[0-9]){10}\\.([A-Z]|[0-9]){8}-([A-Z]|[0-9]){8}\\.([A-Z]|[0-9]){8}@.*";
//	@SuppressWarnings("unused")
//	private JarirEmail findRepliedToEmail(JarirEmail email) {
//		
//		JarirEmail repliedToEmails = null;
//		String inReplyTo = email.getInReplyTo();
//		if(inReplyTo!=null && !inReplyTo.isEmpty()) {
//			for(JarirEmail emailToCheck : emailRepository.findAll()) {
//				String messageIdToCheck = emailToCheck.getMessageId();
//				if(emailService.isMessageIdEquals(inReplyTo, messageIdToCheck)) {
//					repliedToEmails = emailToCheck;
//				}
//			}
//		}
//		
//		return repliedToEmails;
//	}
	
//	private JarirEmail findReferencedEmails(JarirEmail email) {
//		List<JarirEmail> referencedEmails = new ArrayList<>();
//		if(!email.getReferencesEmails().isEmpty()) {
//			String[] references = email.getReferencesEmails().split(" ");
//			for(int i = 0 ; i < references.length; i++) {
//				references[i] = references[i].replace("\n", "").replace("\r", "");
//				for(JarirEmail emailToCheck : emailRepository.findAll()) {
//					String messageIdToCheck = emailToCheck.getMessageId();
//					if(emailService.isMessageIdEquals(references[i], messageIdToCheck)) {
//						referencedEmails.add(emailToCheck);
//					}
//				}
//			}
//		}
//		email.setReferencedEmails(referencedEmails);
//		return email;
//	}
	
//	@Autowired
//	UserRetrievalService userService;
//	@RequestMapping(value = "/getImage/{imageId}", method = RequestMethod.GET)
//	public ResponseEntity<byte[]> getImage(@PathVariable final String imageId) {
//		EmailAttachment attachment = emailAttachmentRepository.findById(Long.parseLong(imageId)).get();
//		try {
//			File file = null;
//			String path = attachment.getFilepath();
//			FileInputStream fileStream = new FileInputStream(
//					file = new File(path));
//			byte[] bytes = new byte[(int) file.length()];
//			fileStream.read(bytes, 0, bytes.length);
//			final HttpHeaders headers = new HttpHeaders();
//			if(URLConnection.guessContentTypeFromName(path)!=null) {
//				headers.setContentType(MediaType.valueOf(URLConnection.guessContentTypeFromName(path)));
//			}
//			fileStream.close();
//			return new ResponseEntity<byte[]>(bytes, headers, HttpStatus.CREATED);
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//		return new ResponseEntity<byte[]>(null, null, HttpStatus.NOT_ACCEPTABLE);
//	}
}
