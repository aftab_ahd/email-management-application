package com.jarir.ems.controllers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import com.jarir.ems.entities.ems.EmailTemplateCategory;
import com.jarir.ems.repos.ems.EmailTemplateCategoryRepository;
import com.jarir.ems.util.ViewHelper;

@Controller
public class TemplatesCategoriesController {
	
	@Autowired
	EmailTemplateCategoryRepository emailTemplateCategoryRepository;
	
	@Autowired
	ViewHelper viewHelper;
	
	@GetMapping(value="/templatesCategories")
	public String showTemplatesPage(Model model) {
		model.addAttribute("templatesCategories", emailTemplateCategoryRepository.findAll());
		model = viewHelper.updateCounts(model);
		return "templatesCategories";
	}
	
	@GetMapping(value="/newtemplatecategorypage")
	public String newTemplatePage(EmailTemplateCategory emailTemplateCategory, Model model) {
		model = viewHelper.updateCounts(model);
		return "emailtemplatescategory/add";
	}
	
	@PostMapping(value="addtemplatecategory")
	public String addTemplate(@Valid EmailTemplateCategory emailTemplateCategory, BindingResult errors, Model model) {
		
		if (errors.hasErrors()) {
			model = viewHelper.updateCounts(model);
            return "emailtemplatescategory/add";
        }

		emailTemplateCategoryRepository.save(emailTemplateCategory);

		return "redirect:/templatesCategories";
	}
	
	@GetMapping("/edittemplatecategory/{id}")
    public String showUpdateForm(@PathVariable("id") long id, Model model) {
		EmailTemplateCategory emailTemplateCategory = emailTemplateCategoryRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid template Id:" + id));
        model.addAttribute("emailTemplateCategory", emailTemplateCategory);
        model = viewHelper.updateCounts(model);
        return "emailtemplatescategory/edit";
    }
    
    @PostMapping("/updatetemplatecategory/{id}")
    public String updateTemplate(@PathVariable("id") long id, @Valid EmailTemplateCategory emailTemplateCategory, BindingResult result, Model model) {
        if (result.hasErrors()) {
        	emailTemplateCategory.setId(id);
        	model = viewHelper.updateCounts(model);
            return "emailtemplatescategory/edit";
        }

		emailTemplateCategoryRepository.save(emailTemplateCategory);

        return "redirect:/templatesCategories";
    }
    
    @GetMapping("/deletetemplatecategory/{id}")
    public String deleteTemplate(@PathVariable("id") long id, Model model) {
    	EmailTemplateCategory emailTemplateCategory = emailTemplateCategoryRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid template Id:" + id));
    	emailTemplateCategoryRepository.delete(emailTemplateCategory);
        return "redirect:/templatesCategories";
    }

}
