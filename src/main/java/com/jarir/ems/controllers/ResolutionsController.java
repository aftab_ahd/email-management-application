package com.jarir.ems.controllers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import com.jarir.ems.entities.ems.EmailResolution;
import com.jarir.ems.repos.ems.EmailResolutionRepository;
import com.jarir.ems.util.ViewHelper;

@Controller
public class ResolutionsController {
	
	@Autowired
	EmailResolutionRepository emailResolutionRepository;
	
	@Autowired
	ViewHelper viewHelper;
	
	@GetMapping(value="/resolutions")
	public String showResolutionsPage(Model model) {
		model.addAttribute("resolutions", emailResolutionRepository.findAll());
		model = viewHelper.updateCounts(model);
		return "resolutions";
	}
	
	@GetMapping(value="/newresolutionpage")
	public String newResolutionPage(EmailResolution emailResolution, Model model) {
		model = viewHelper.updateCounts(model);
		return "emailresolutions/add";
	}
	
	@PostMapping(value="addresolution")
	public String addResolution(@Valid EmailResolution emailResolution, BindingResult errors, Model model) {
		
		if (errors.hasErrors()) {
			model = viewHelper.updateCounts(model);
            return "emailresolutions/add";
        }
         
		emailResolutionRepository.save(emailResolution);

		return "redirect:/resolutions";
	}
	
	@GetMapping("/editresolution/{id}")
    public String showUpdateForm(@PathVariable("id") long id, Model model) {
		EmailResolution emailResolution = emailResolutionRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid resolution Id:" + id));
        model.addAttribute("emailResolution", emailResolution);
        model = viewHelper.updateCounts(model);
        return "emailresolutions/edit";
    }
    
    @PostMapping("/updateresolution/{id}")
    public String updateResolution(@PathVariable("id") long id, @Valid EmailResolution emailResolution, BindingResult result, Model model) {
        if (result.hasErrors()) {
        	model = viewHelper.updateCounts(model);
        	emailResolution.setId(id);
            return "emailresolutions/edit";
        }
        
        emailResolutionRepository.save(emailResolution);

        return "redirect:/resolutions";
    }
    
    @GetMapping("/deleteresolution/{id}")
    public String deleteResolution(@PathVariable("id") long id, Model model) {
    	EmailResolution emailResolution = emailResolutionRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid resolution Id:" + id));
        emailResolutionRepository.delete(emailResolution);
        return "redirect:/resolutions";
    }

}
