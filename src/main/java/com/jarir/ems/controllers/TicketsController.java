package com.jarir.ems.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import com.jarir.ems.repos.ems.TicketRepository;
import com.jarir.ems.util.ViewHelper;

@Controller
public class TicketsController {
	
	@Autowired
	TicketRepository ticketRepo;
	
	@Autowired
	ViewHelper viewHelper;
	
	@GetMapping("/pendingtickets")
	public String showPendingTickets(Model model) {
		model.addAttribute("tickets", ticketRepo.findAllByStatus("Pending"));
		model = viewHelper.updateCounts(model);
		return "tickets/pendingTickets";
	}
	
	@GetMapping("/closedtickets")
	public String showClosedTickets(Model model) {
		model.addAttribute("tickets", ticketRepo.findAllByStatus("Closed"));
		model = viewHelper.updateCounts(model);
		return "tickets/closedTickets";
	}
	
}
