package com.jarir.ems.controllers;

import java.io.IOException;
import java.util.Arrays;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.jarir.ems.repos.ems.JarirEmailRepository;
import com.jarir.ems.services.ReportingService;
import com.jarir.ems.util.ViewHelper;


@Controller
public class HomeController {

	@Autowired
	ReportingService reporting;
	
	@Autowired
	JarirEmailRepository emailRepository;
	
	@Autowired
	ViewHelper viewHelper;
	
	@GetMapping("/")
	protected String index(Model model) {
		return "redirect:/home";
	}
	
	@GetMapping("/home")
	public String home(Model model) {		
		model.addAttribute("emailsMonthly", reporting.getTotalReceivedEmailsForCurrentMonth().size());
		model.addAttribute("actionsMonthly", reporting.getTotalActionsForCurrentMonth().size());
		 
//		int allEmailsCount = emailRepository.findAll().size();
		long allEmailsCount = emailRepository.count();
		long actionedEmailsCount = emailRepository.countByStatusNot("Pending");
//		int actionedEmailsCount = emailRepository.findByStatusNot("Pending").size();
		
		double pendingPercentage = allEmailsCount == 0 ? 0d : ((allEmailsCount-actionedEmailsCount)/new Double(allEmailsCount) * 100);
		model.addAttribute("pendingPercentage", String.format("%.2f", pendingPercentage));
		model.addAttribute("pendingEmails", emailRepository.findByStatus("Pending").size());
		
		model.addAttribute("totalsAnnual", Arrays.toString(reporting.allYearEmailCountPerMonth()));
		
		String[] resolutionsAndCount = reporting.resolutionsAndCount();
		model.addAttribute("resolutionsLabels", splitResolutionAndCountStringArray(resolutionsAndCount, 0));
		model.addAttribute("resolutionsData", splitResolutionAndCountStringArray(resolutionsAndCount, 1));
		model = viewHelper.updateCounts(model);
		return "index";
	}
	
	private String splitResolutionAndCountStringArray(String[] resolutionsAndCount, int token) {
		String[] data = new String[resolutionsAndCount.length];
		int index = 0;
		for(String line : resolutionsAndCount) {
			data[index] = line.split(":")[token];
			index++;
		}

		return Arrays.toString(data);
	}

	@GetMapping(value="/generateReport", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public void generateReport(@RequestParam("type") String type, @RequestParam int month, @RequestParam int year, HttpServletResponse response) {
		response.setHeader("Content-disposition", "attachment; filename=report.xlsx");
		response.setContentType("application/force-download");
		try {
			reporting.generateExcelReport(type, month, year, response.getOutputStream());
			response.flushBuffer();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
	
	@GetMapping(value = {"/logout"})
	public String logoutDo(HttpServletRequest request,HttpServletResponse response){
		try {
			request.logout();
			request.getSession().invalidate();
		} catch (ServletException e) {
			e.printStackTrace();
		}
		return "redirect:/";
	}
}
