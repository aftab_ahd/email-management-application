package com.jarir.ems.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import com.jarir.ems.entities.ems.AutoEmail;
import com.jarir.ems.repos.ems.AutoEmailRepository;
import com.jarir.ems.repos.ems.EmailTemplateCategoryRepository;
import com.jarir.ems.repos.ems.EmailTemplateRepository;
import com.jarir.ems.util.ViewHelper;

@Controller
public class AutoEmailsController {
	
	@Autowired
	EmailTemplateRepository emailTemplateRepository;
	
	@Autowired
	EmailTemplateCategoryRepository emailTemplateCategoryRepository;
	
	@Autowired
	AutoEmailRepository autoEmailRepository;
	
	@Autowired
	ViewHelper viewHelper;
	
	@GetMapping(value="/showautoreply")
	public String showAutoReplyPage(Model model) {
		model.addAttribute("autoEmail", autoEmailRepository.findOneByName("auto-reply"));
		model = viewHelper.updateCounts(model);
		return "autoReply";
	}
	
	@PostMapping("/updateautoreply/{id}")
	public String updateAutoReply(@PathVariable("id") long id, AutoEmail autoEmail, BindingResult result, Model model) {
		
		if(autoEmail.getAraBody().isEmpty())
			result.addError(new ObjectError("araBody", "Value is Required"));
		
		if(autoEmail.getEngBody().isEmpty())
			result.addError(new ObjectError("engBody", "Value is Required"));
		
		if (result.hasErrors()) {
			model = viewHelper.updateCounts(model);
			return "autoReply";
		}
		autoEmailRepository.save(autoEmail);

		return "redirect:/showautoreply";
	}
	
	@GetMapping(value="/showforwardto")
	public String showForwardtoPage(Model model) {
		model.addAttribute("autoEmail", autoEmailRepository.findOneByName("forward-to"));
		model = viewHelper.updateCounts(model);
		return "forwardTo";
	}
	
	@PostMapping("/updateforwardto/{id}")
	public String updateForwardTo(@PathVariable("id") long id, AutoEmail autoEmail, BindingResult result, Model model) {
		
		if(autoEmail.getEngBody().isEmpty())			
			result.addError(new ObjectError("engBody", "Value is Required"));
		
		if (result.hasErrors()) {
			model = viewHelper.updateCounts(model);
			return "forwardTo";
		}
		autoEmailRepository.save(autoEmail);

		return "redirect:/showforwardto";
	}

}
