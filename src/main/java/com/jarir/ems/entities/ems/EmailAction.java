package com.jarir.ems.entities.ems;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;

@Entity
public class EmailAction {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
	@GenericGenerator(name = "native", strategy = "native")
	private Long id;
	
	@OneToOne(mappedBy = "emailAction", fetch= FetchType.LAZY)
	private JarirEmail jarirEmail;
	
	@OneToOne//(cascade = CascadeType.ALL)
    @JoinColumn(name = "jarirTemplate_id", referencedColumnName = "id")
	private EmailTemplate emailTemplate;
	
	@OneToOne//(cascade = CascadeType.MERGE)
    @JoinColumn(name = "jarirResolution_id", referencedColumnName = "id")
	private EmailResolution emailResolution;
	
	private String language;
	
	@OneToMany(mappedBy="emailAction", cascade = CascadeType.ALL)
	private List<EmailTemplateParameter> parameters;
	
	private String forwardTo;
	
	private String userId;

	@CreationTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public EmailTemplate getEmailTemplate() {
		return emailTemplate;
	}

	public void setEmailTemplate(EmailTemplate emailTemplate) {
		this.emailTemplate = emailTemplate;
	}

	public EmailResolution getEmailResolution() {
		return emailResolution;
	}

	public void setEmailResolution(EmailResolution emailResolution) {
		this.emailResolution = emailResolution;
	}

	public JarirEmail getJarirEmail() {
		return jarirEmail;
	}

	public void setJarirEmail(JarirEmail jarirEmail) {
		this.jarirEmail = jarirEmail;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public List<EmailTemplateParameter> getParameters() {
		return parameters;
	}

	public void setParameters(List<EmailTemplateParameter> parameters) {
		this.parameters = parameters;
	}

	public String getForwardTo() {
		return forwardTo;
	}

	public void setForwardTo(String forwardTo) {
		this.forwardTo = forwardTo;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

}
