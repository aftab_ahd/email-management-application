package com.jarir.ems.entities.ems;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UpdateTimestamp;

@Entity
public class Ticket {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
	@GenericGenerator(name = "native", strategy = "native")
	private Long id;
	
//	@Version                         
//    private Long version = null;
	
	@OneToMany(fetch = FetchType.EAGER, mappedBy="ticket", cascade = CascadeType.ALL)
	private List<JarirEmail> emails = new ArrayList<>();
	
	@OneToMany(mappedBy="ticket", cascade = CascadeType.ALL)
	private List<Comment> comments = new ArrayList<>();
	
	@OneToMany(mappedBy="ticket", cascade = CascadeType.ALL)
	private List<TicketAction> actions = new ArrayList<>();

	@CreationTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;
	
	@UpdateTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateDate;
	
	private String closedBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date closedDate;

	//Pending - Closed - FollowUp
	//Close Ticket - Set to Follow Up - Re-open Ticket -
	private String status;
	
	private Boolean hasUpdates;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

//	public Long getVersion() {
//		return version;
//	}
//
//	public void setVersion(Long version) {
//		this.version = version;
//	}

	public List<JarirEmail> getEmails() {
		return emails;
	}

	public void setEmails(List<JarirEmail> emails) {
		this.emails = emails;
	}

	public List<Comment> getComments() {
		return comments;
	}

	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getClosedBy() {
		return closedBy;
	}

	public void setClosedBy(String closedBy) {
		this.closedBy = closedBy;
	}

	public Date getClosedDate() {
		return closedDate;
	}

	public void setClosedDate(Date closedDate) {
		this.closedDate = closedDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Boolean getHasUpdates() {
		return hasUpdates;
	}

	public void setHasUpdates(Boolean hasUpdates) {
		this.hasUpdates = hasUpdates;
	}

	public List<TicketAction> getActions() {
		return actions;
	}

	public void setActions(List<TicketAction> actions) {
		this.actions = actions;
	}
	
}
