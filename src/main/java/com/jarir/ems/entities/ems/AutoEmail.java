package com.jarir.ems.entities.ems;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.GenericGenerator;

@Entity
public class AutoEmail {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
	@GenericGenerator(name = "native", strategy = "native")
	private Long id;

	private String name;
	private String araBody;
	private String engBody;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAraBody() {
		return araBody;
	}

	public void setAraBody(String araBody) {
		this.araBody = araBody;
	}

	public String getEngBody() {
		return engBody;
	}

	public void setEngBody(String engBody) {
		this.engBody = engBody;
	}

}
