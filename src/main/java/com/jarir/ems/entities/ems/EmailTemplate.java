package com.jarir.ems.entities.ems;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotBlank;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class EmailTemplate {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
	@GenericGenerator(name = "native", strategy = "native")
	private Long id;

	@NotBlank(message = "Template Name is Mandatory")
	private String name;
	
	@NotBlank(message = "English Body is Mandatory")
	@Column(columnDefinition = "TEXT")
	private String bodyEnglish;
	
	@NotBlank(message = "Arabic Body is Mandatory")
	@Column(columnDefinition = "TEXT")
	private String bodyArabic;
	
	private int parametersCountEng;
	private int parametersCountAra;
	
	@ManyToOne(fetch = FetchType.LAZY/*, cascade=CascadeType.ALL*/, optional = false)
	@JoinColumn(name = "emailTemplateCategory_id", nullable = false)
	@JsonIgnore
	private EmailTemplateCategory emailTemplateCategory;
	
//	@OneToMany(mappedBy="emailTemplate", cascade = CascadeType.ALL)
//	private List<EmailTemplateParameter> parameters;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getBodyEnglish() {
		return bodyEnglish;
	}

	public void setBodyEnglish(String bodyEnglish) {
		this.bodyEnglish = bodyEnglish;
	}

	public String getBodyArabic() {
		return bodyArabic;
	}

	public void setBodyArabic(String bodyArabic) {
		this.bodyArabic = bodyArabic;
	}

	public int getParametersCountEng() {
		return parametersCountEng;
	}

	public void setParametersCountEng(int parametersCountEng) {
		this.parametersCountEng = parametersCountEng;
	}

	public int getParametersCountAra() {
		return parametersCountAra;
	}

	public void setParametersCountAra(int parametersCountAra) {
		this.parametersCountAra = parametersCountAra;
	}

	public EmailTemplateCategory getEmailTemplateCategory() {
		return emailTemplateCategory;
	}

	public void setEmailTemplateCategory(EmailTemplateCategory emailTemplateCategory) {
		this.emailTemplateCategory = emailTemplateCategory;
	}

}
