package com.jarir.ems.entities.ems;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UpdateTimestamp;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class JarirEmail {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
	@GenericGenerator(name = "native", strategy = "native")
	private Long id;
	
//	@Version                         
//    private Long version = null;
	
	@Column(updatable = false, nullable = false)
    private String uuid;

	private String fromEmail;
	
	@Column(columnDefinition = "TEXT")
	private String toEmail;
	
	@Column(columnDefinition = "TEXT")
	private String ccEmail;
	
	@Column(columnDefinition = "TEXT")
	private String subject;
	
	@Column(columnDefinition = "LONGTEXT")
	private String body;
	
	@Column(columnDefinition = "TEXT")
	private String bodyHtml;
	
	private Long emailUid;
	
	private String messageId;
	
	//Pending - Replied - Forward To - Junk/No Action
	@Column(length = 15)
	private String status;
	
	@OneToOne(cascade = {CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE})
    @JoinColumn(name = "emailAction_id", referencedColumnName = "id")
//	@JsonIgnore
	EmailAction emailAction;
	
	@Transient
	@JsonIgnore
	private List<JarirEmail> referencedEmails;
	
	@Column(columnDefinition = "TEXT")
	private String referencesEmails;
	
	@Column(columnDefinition = "TEXT")
	private String inReplyTo;
	
	private String replyTo;
	
	//List of images in email?
	
	@CreationTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date sentDate;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date receivedDate;
	
	@UpdateTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateDate;
	
	@OneToMany(mappedBy="jarirEmail", cascade = CascadeType.ALL)
	private List<EmailAttachment> emailAttachments = new ArrayList<>();
	
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "emailAccount_id", nullable = false)
	@JsonIgnore
	private EmailAccount emailAccount;
	
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "ticket_id")
	@JsonIgnore
	private Ticket ticket;
	
	public JarirEmail() {
		super();
		this.uuid = UUID.randomUUID().toString();
	}

	public Long getId() {
		return id;
	}

	public void setId(final Long id) {
		this.id = id;
	}

//	public Long getVersion() {
//		return version;
//	}
//
//	public void setVersion(Long version) {
//		this.version = version;
//	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getFromEmail() {
		return fromEmail;
	}

	public void setFromEmail(String fromEmail) {
		this.fromEmail = fromEmail;
	}

	public String getToEmail() {
		return toEmail;
	}

	public void setToEmail(String toEmail) {
		this.toEmail = toEmail;
	}

	public String getCcEmail() {
		return ccEmail;
	}

	public void setCcEmail(String ccEmail) {
		this.ccEmail = ccEmail;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public String getBodyHtml() {
		return bodyHtml;
	}

	public void setBodyHtml(String bodyHtml) {
		this.bodyHtml = bodyHtml;
	}

	public Long getEmailUid() {
		return emailUid;
	}

	public void setEmailUid(Long emailUid) {
		this.emailUid = emailUid;
	}

	public String getMessageId() {
		return messageId;
	}

	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public EmailAction getEmailAction() {
		return emailAction;
	}

	public void setEmailAction(EmailAction emailAction) {
		this.emailAction = emailAction;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getSentDate() {
		return sentDate;
	}

	public void setSentDate(Date sentDate) {
		this.sentDate = sentDate;
	}

	public Date getReceivedDate() {
		return receivedDate;
	}

	public void setReceivedDate(Date receivedDate) {
		this.receivedDate = receivedDate;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public List<EmailAttachment> getEmailAttachments() {
		return emailAttachments;
	}

	public void setEmailAttachments(List<EmailAttachment> emailAttachments) {
		this.emailAttachments = emailAttachments;
	}
	
	public void addToEmailAttachments(EmailAttachment emailAttachment) {
		this.emailAttachments.add(emailAttachment);
	}

	public String getReferencesEmails() {
		return referencesEmails;
	}

	public void setReferencesEmails(String referencesEmails) {
		this.referencesEmails = referencesEmails;
	}

	public String getInReplyTo() {
		return inReplyTo;
	}

	public void setInReplyTo(String inReplyTo) {
		this.inReplyTo = inReplyTo;
	}

	public String getReplyTo() {
		return replyTo;
	}

	public void setReplyTo(String replyTo) {
		this.replyTo = replyTo;
	}

	public EmailAccount getEmailAccount() {
		return emailAccount;
	}

	public void setEmailAccount(EmailAccount emailAccount) {
		this.emailAccount = emailAccount;
	}

	public List<JarirEmail> getReferencedEmails() {
		return referencedEmails;
	}

	public void setReferencedEmails(List<JarirEmail> referencedEmails) {
		this.referencedEmails = referencedEmails;
	}

	public Ticket getTicket() {
		return ticket;
	}

	public void setTicket(Ticket ticket) {
		this.ticket = ticket;
	}
	
}