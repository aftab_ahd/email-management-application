package com.jarir.ems.repos.ems;

import org.springframework.data.jpa.repository.JpaRepository;

import com.jarir.ems.entities.ems.TicketAction;

public interface TicketActionRepository extends JpaRepository<TicketAction, Long>  {

}
