package com.jarir.ems.repos.ems;

import org.springframework.data.jpa.repository.JpaRepository;

import com.jarir.ems.entities.ems.EmailAttachment;

public interface EmailAttachmentRepository extends JpaRepository<EmailAttachment, Long>  {

}
