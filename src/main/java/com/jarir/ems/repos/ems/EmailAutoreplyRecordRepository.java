package com.jarir.ems.repos.ems;

import org.springframework.data.jpa.repository.JpaRepository;

import com.jarir.ems.entities.ems.EmailAutoreplyRecord;
import com.jarir.ems.entities.ems.JarirEmail;
import java.util.List;

public interface EmailAutoreplyRecordRepository extends JpaRepository<EmailAutoreplyRecord, Long>  {
	List<EmailAutoreplyRecord> findByJarirEmail(JarirEmail jariremail);
}