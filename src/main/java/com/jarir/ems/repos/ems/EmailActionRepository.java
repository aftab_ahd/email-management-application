package com.jarir.ems.repos.ems;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.jarir.ems.entities.ems.EmailAction;
import com.jarir.ems.entities.ems.EmailResolution;

public interface EmailActionRepository extends JpaRepository<EmailAction, Long> {

	List<EmailAction> findAllByEmailResolution(EmailResolution emailresolution);

	@Query("SELECT e FROM EmailAction e where year(e.createDate) = year(current_date) and  month(e.createDate) = month(current_date)")
	List<EmailAction> getAllActionsOfCurrentMonth();

	@Query("SELECT e FROM EmailAction e where year(e.createDate) = year(current_date) and  month(e.createDate) = month(current_date) and week(e.createDate) = week(current_date)")
	List<EmailAction> getAllActionsOfCurrentWeek();

	@Query("SELECT e FROM EmailAction e where year(e.createDate) = year(current_date) and  month(e.createDate) = month(current_date) and week(e.createDate) = week(current_date) and day(e.createDate) = day(current_date)")
	List<EmailAction> getAllActionsOfToday();

	@Query("SELECT e FROM EmailAction e where year(e.createDate) = ?2 and  month(e.createDate) = ?1")
	List<EmailAction> getAllActionsOfMonthAndYear(int month, int year);
	
	@Query("SELECT e FROM EmailAction e where year(e.createDate) = ?2 and  month(e.createDate) = ?1 and jarirResolution_id is NULL")
	List<EmailAction> getAllNoActionOfMonthAndYear(int month, int year);

	// Does not include NULL resolutions = 
	@Query("SELECT count(*) FROM EmailAction e JOIN JarirEmail j on e.id = j.emailAction.id and j.emailAccount.id = ?3 where year(e.createDate) = ?1 and  month(e.createDate) = ?2")
	int countByYearAndMonthAndAccount(int year, int month, Long accountId);
	
	@Query("SELECT count(*) FROM EmailAction e JOIN JarirEmail j on e.id = j.emailAction.id and j.emailAccount.id = ?3 where year(e.createDate) = ?1 and  month(e.createDate) = ?2  and jarirResolution_id is NULL")
	int countByYearAndMonthAndAccountAndNullResolution(int year, int month, Long accountId);

	@Query("SELECT e FROM EmailAction e where year(e.createDate) = ?3 and month(e.createDate) = ?2 and day(e.createDate) = ?1")
	List<EmailAction> getAllActionsOfDayAndMonthAndYear(int day, int month, int year);
	
	@Query("SELECT e FROM EmailAction e where year(e.createDate) = ?3 and month(e.createDate) = ?2 and day(e.createDate) = ?1 and jarirResolution_id is NULL")
	List<EmailAction> getAllNoActionOfDayAndMonthAndYear(int day, int month, int year);

	@Query("SELECT count(*) FROM EmailAction e JOIN JarirEmail j on e.id = j.emailAction.id and j.emailAccount.id = ?4 where year(e.createDate) = ?1 and  month(e.createDate) = ?2 and day(e.createDate) = ?3")
	int countByDateAndAccount(int year, int month, int day, Long accountId);
	
	@Query("SELECT count(*) FROM EmailAction e JOIN JarirEmail j on e.id = j.emailAction.id and j.emailAccount.id = ?4 where year(e.createDate) = ?1 and  month(e.createDate) = ?2 and day(e.createDate) = ?3 and jarirResolution_id is NULL")
	int countByDateAndAccountAndNullResolution(int year, int month, int day, Long accountId);

	@Query("SELECT e FROM EmailAction e where year(e.createDate) = ?3 and month(e.createDate) = ?2 and day(e.createDate) = ?1")
	List<EmailAction> getAllActionsOfDayAndMonthAndYearAndAccount(int day, int month, int year, Long accountId);

	@Query("SELECT e FROM EmailAction e where year(e.createDate) = ?1 and  month(e.createDate) = ?2 and day(e.createDate) = ?3 and jarirResolution_id = ?4")
	List<EmailAction> getAllByResolutionAndDate(int year, int month, int day, Long resolutionId);

	@Query("SELECT count(*) FROM EmailAction e JOIN JarirEmail j on e.id = j.emailAction.id and j.emailAccount.id = ?5 where year(e.createDate) = ?1 and  month(e.createDate) = ?2 and day(e.createDate) = ?3 and jarirResolution_id = ?4")
	int countByResolutionAndDateAndAccount(int year, int month, int day, Long resolutionId, Long accountId);

	@Query("SELECT e FROM EmailAction e where year(e.createDate) = ?1 and  month(e.createDate) = ?2 and jarirResolution_id = ?3")
	List<EmailAction> getAllByResolutionAndYearAndMonth(int year, int month, Long resolutionId);

	@Query("SELECT count(*) FROM EmailAction e JOIN JarirEmail j on e.id = j.emailAction.id and j.emailAccount.id = ?4 where year(e.createDate) = ?1 and  month(e.createDate) = ?2 and jarirResolution_id = ?3")
	int countByResolutionAndYearAndMonthAndAccount(int year, int month, Long resolutionId, Long accountId);

	@Query("SELECT e FROM EmailAction e where year(e.createDate) = ?1 and  jarirResolution_id = ?2")
	List<EmailAction> getAllByResolutionAndYear(int year, Long resolutionId);

	@Query("SELECT count(*) FROM EmailAction e JOIN JarirEmail j on e.id = j.emailAction.id and j.emailAccount.id = ?3  where year(e.createDate) = ?1 and  jarirResolution_id = ?2")
	int countByResolutionAndYearAndAccount(int year, Long resolutionId, Long accountId);

	@Query("SELECT count(*) FROM EmailAction e JOIN JarirEmail j on e.id = j.emailAction.id and j.emailAccount.id = ?2 where year(e.createDate) = ?1")
	int countByYearAndAccount(int year, Long accountId);
	
	@Query("SELECT count(*) FROM EmailAction e JOIN JarirEmail j on e.id = j.emailAction.id and j.emailAccount.id = ?2 where year(e.createDate) = ?1  and jarirResolution_id is NULL")
	int countByYearAndAccountAndNullResolution(int year, Long accountId);

	@Query("SELECT e FROM EmailAction e where year(e.createDate) = ?1")
	List<EmailAction> getAllActionsOfYear(int year);
	
	@Query("SELECT e FROM EmailAction e where year(e.createDate) = ?1 and jarirResolution_id is NULL")
	List<EmailAction> getAllNoActionOfYear(int year);

}
