package com.jarir.ems.repos.ems;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.jarir.ems.entities.ems.JarirEmail;

public interface JarirEmailRepository extends JpaRepository<JarirEmail, Long> {
	JarirEmail findOneByEmailUid(Long emailuid);

	JarirEmail findOneByMessageId(String messageId);

	List<JarirEmail> findByEmailActionIsNull();

	List<JarirEmail> findByStatus(String status);

	long countByStatusNot(String status);
	long countByStatus(String status);
	
	List<JarirEmail> findByStatusNot(String status);

	List<JarirEmail> findByStatusIsNull();
	List<JarirEmail> findByStatusIsNotNull();
	List<JarirEmail> findByStatusContains(String status);

//	@Query("SELECT a from JarirEmail a WHERE receivedDate > DATE_SUB(now(), INTERVAL 1 DAY) AND fromEmail = :fromEmail")
//	List<JarirEmail> findByfromEmailForLastDay(@Param("fromEmail") String fromEmail);

	List<JarirEmail> findAllByFromEmailAndReceivedDateBetween(String fromEmail, Date from, Date to);

	List<JarirEmail> findAllByReceivedDateBetween(Date from, Date to);

	@Query("SELECT e FROM JarirEmail e where year(e.receivedDate) = ?2 and month(e.receivedDate) = ?1")
	List<JarirEmail> getAllEmailsOfMonthAndYear(int month, int year);
	
	@Query("SELECT e FROM JarirEmail e where year(e.receivedDate) = ?3 and month(e.receivedDate) = ?2 and day(e.receivedDate) = ?1 and emailAccount_id = ?4")
	List<JarirEmail> getAllEmailsOfDayAndMonthAndYearAndAccount(int day, int month, int year, Long accountId);
	
	@Query("SELECT e FROM JarirEmail e where year(e.receivedDate) = ?2 and month(e.receivedDate) = ?1 and emailAccount_id = ?3")
	List<JarirEmail> getAllEmailsOfMonthAndYearAndAccount(int month, int year, Long accountId);
	
	@Query("SELECT e FROM JarirEmail e where year(e.receivedDate) = ?1 and emailAccount_id = ?2")
	List<JarirEmail> getAllEmailsOfYearAndAccount(int year, Long accountId);
	
	@Query("SELECT e FROM JarirEmail e where year(e.receivedDate) = ?3 and month(e.receivedDate) = ?2 and day(e.receivedDate) = ?1")
	List<JarirEmail> getAllEmailsOfDayAndMonthAndYear(int day, int month, int year);
	
	@Query("SELECT e FROM JarirEmail e where year(e.receivedDate) = year(current_date) and  month(e.receivedDate) = month(current_date)")
	List<JarirEmail> getAllEmailsOfCurrentMonth();

	@Query("SELECT e FROM JarirEmail e where year(e.receivedDate) = year(current_date) and  month(e.receivedDate) = month(current_date) and week(e.receivedDate) = week(current_date)")
	List<JarirEmail> getAllEmailsOfCurrentWeek();

	@Query("SELECT e FROM JarirEmail e where year(e.receivedDate) = year(current_date) and  month(e.receivedDate) = month(current_date) and week(e.receivedDate) = week(current_date) and day(e.receivedDate) = day(current_date)")
	List<JarirEmail> getAllEmailsOfToday();

	@Query("SELECT e FROM JarirEmail e where year(e.receivedDate) = ?1")
	List<JarirEmail> getAllEmailsOfYear(int year);

}
