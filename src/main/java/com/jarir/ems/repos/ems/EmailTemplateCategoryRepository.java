package com.jarir.ems.repos.ems;

import org.springframework.data.jpa.repository.JpaRepository;

import com.jarir.ems.entities.ems.EmailTemplateCategory;

public interface EmailTemplateCategoryRepository extends JpaRepository<EmailTemplateCategory, Long>  {

}
