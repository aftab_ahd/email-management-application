package com.jarir.ems.repos.ems;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.jarir.ems.entities.ems.Ticket;

public interface TicketRepository extends JpaRepository<Ticket, Long>  {
	List<Ticket> findAllByStatus(String status);
	List<Ticket> findAllByStatusNot(String status);
}
