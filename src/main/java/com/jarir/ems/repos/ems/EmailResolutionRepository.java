package com.jarir.ems.repos.ems;

import org.springframework.data.jpa.repository.JpaRepository;

import com.jarir.ems.entities.ems.EmailResolution;

public interface EmailResolutionRepository extends JpaRepository<EmailResolution, Long>  {
	EmailResolution findOneByName(String name);
}
