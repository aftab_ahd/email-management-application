package com.jarir.ems.repos.ems;

import org.springframework.data.jpa.repository.JpaRepository;

import com.jarir.ems.entities.ems.EmailTemplate;
import com.jarir.ems.entities.ems.EmailTemplateCategory;
import java.util.List;

public interface EmailTemplateRepository extends JpaRepository<EmailTemplate, Long>  {
	List<EmailTemplate> findByEmailTemplateCategory(EmailTemplateCategory emailtemplatecategory);
}
