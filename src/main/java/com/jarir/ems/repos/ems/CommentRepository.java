package com.jarir.ems.repos.ems;

import org.springframework.data.jpa.repository.JpaRepository;

import com.jarir.ems.entities.ems.Comment;

public interface CommentRepository extends JpaRepository<Comment, Long>  {

}
