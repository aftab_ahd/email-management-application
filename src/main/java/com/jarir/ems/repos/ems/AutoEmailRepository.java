package com.jarir.ems.repos.ems;

import org.springframework.data.jpa.repository.JpaRepository;

import com.jarir.ems.entities.ems.AutoEmail;

public interface AutoEmailRepository extends JpaRepository<AutoEmail, Long> {
	AutoEmail findOneByName(String name);
}
