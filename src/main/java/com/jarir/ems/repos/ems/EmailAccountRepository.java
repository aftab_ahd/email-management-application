package com.jarir.ems.repos.ems;

import org.springframework.data.jpa.repository.JpaRepository;

import com.jarir.ems.entities.ems.EmailAccount;

public interface EmailAccountRepository extends JpaRepository<EmailAccount, Long>  {
	EmailAccount findOneByUsername(String username);
}
