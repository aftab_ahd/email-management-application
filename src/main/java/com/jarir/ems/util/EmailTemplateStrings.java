package com.jarir.ems.util;

public class EmailTemplateStrings {
	private String emailTemplateStart = "<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" height=\"100%\"\r\n" + 
			"	width=\"100%\">\r\n" + 
			"	<tbody>\r\n" + 
			"		<tr>\r\n" + 
			"			<td align=\"center\" valign=\"top\" style=\"padding: 20px 0 20px 0\">\r\n" + 
			"				<!-- [ header starts here] -->\r\n" + 
			"				<table bgcolor=\"#FFFFFF\" cellspacing=\"0\" cellpadding=\"3\" border=\"0\"\r\n" + 
			"					width=\"768\"\r\n" + 
			"					style=\"border: 1px solid #E0E0E0; padding: 10px; border-radius: 8px;\">\r\n" + 
			"					<tbody>\r\n" + 
			"						<tr bgcolor=\"#ffffff\">\r\n" + 
			"							<td valign=\"top\">\r\n" + 
			"								<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"\r\n" + 
			"									style=\"\">\r\n" + 
			"									<tbody>\r\n" + 
			"										<tr style=\"width: 100%; text-align: center;\">\r\n" + 
			"											<td style=\"width: 100%; padding: 5px;\" valign=\"top\"\r\n" + 
			"												align=\"center\"><img style=\"width: 738px;\"\r\n" + 
			"												src=\"http://www.jarir.com/media/wysiwyg/Saudi/email/benefitsBar.png\">\r\n" + 
			"											</td>\r\n" + 
			"										</tr>\r\n" + 
			"									</tbody>\r\n" + 
			"								</table>\r\n" + 
			"							</td>\r\n" + 
			"						</tr>\r\n" + 
			"						<tr bgcolor=\"#FFFFFF\">\r\n" + 
			"							<td valign=\"top\" align=\"center\"><img\r\n" + 
			"								src=\"http://www.jarir.com/media/wysiwyg/Saudi/Email_Template_Assests/JarirLogoTrans.png\"\r\n" + 
			"								alt=\"Heading\" width=\"370\" height=\"34\"\r\n" + 
			"								style=\"display: block; margin-top: 10px;\"></td>\r\n" + 
			"						</tr>\r\n" + 
			"						<tr bgcolor=\"#ffffff\">\r\n" + 
			"							<td valign=\"top\">\r\n" + 
			"								<table style=\"min-height: 250px;\" width=\"100%\" align=\"center\" border=\"0\" cellspacing=\"0\"\r\n" + 
			"									cellpadding=\"10\">\r\n" + 
			"									<tbody>\r\n" + 
			"										<tr>\r\n" + 
			"											<td valign=\"top\" ${align}\r\n" + 
			"												style=\"${arabic_style_template}vertical-align: middle;font-family: 'Tajawal', Helvetica, Arial, sans-serif; font-size: 22px; line-height: 26px;\">\r\n" + 
			"												<p>";
	private String emailTemplateEnd = "</p>\r\n" + 
			"											</td>\r\n" + 
			"										</tr>\r\n" +
//			"										<tr>\r\n" +
//			"											<td style=\"${arabic_style_body}vertical-align: middle;font-family: 'Tajawal', Helvetica, Arial, sans-serif; font-size: 12px; line-height: 26px;padding: 0.01em 16px;\">\r\n" +
//			"												<p>${original_email_body}</p>" +
//			"											</td>\r\n" +
//			"										</tr>\r\n" +
			"									</tbody>\r\n" + 
			"								</table>\r\n" + 
			"								<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"10\"\r\n" + 
			"									style=\"background: #3C4858; padding: 10px; border-radius: 8px;\">\r\n" + 
			"									<tbody>\r\n" + 
			"										<tr style=\"background: #3C4858\">\r\n" + 
			"											<td\r\n" + 
			"												style=\"width: 30%; font-family: 'Tajawal', Helvetica, Arial, sans-serif; font-size: 16.0px; color: rgba(255, 255, 255, 1.0); text-align: center;\">\r\n" + 
			"												<p\r\n" + 
			"													style=\"font-family: 'Tajawal', Helvetica, Arial, sans-serif; font-size: 13.0px;\">\r\n" + 
			"													<strong>تــحــتــاج مــســاعــدة؟<br>N E E\r\n" + 
			"														D&nbsp;&nbsp;&nbsp;H E L P?\r\n" + 
			"													</strong>\r\n" + 
			"												</p> <a href=\"tel:920000089\"\r\n" + 
			"												style=\"font-family: 'Tajawal', Helvetica, Arial, sans-serif; text-decoration: none; color: #FFF; font-size: 26.0px;\"><img\r\n" + 
			"													width=\"13%\" style=\"position: relative; top: 5px;\"\r\n" + 
			"													src=\"http://www.jarir.com/media/wysiwyg/Saudi/email/phone.png\">&nbsp;<strong>920000089</strong></a>\r\n" + 
			"												<br>\r\n" + 
			"											<br> <a href=\"mailto:care@jarir.com\"\r\n" + 
			"												style=\"font-family: 'Tajawal', Helvetica, Arial, sans-serif; text-decoration: none; color: #FFF; font-size: 20.0px;\"><img\r\n" + 
			"													width=\"10%\" style=\"position: relative; top: 2px;\"\r\n" + 
			"													src=\"http://www.jarir.com/media/wysiwyg/Saudi/email/email.png\">&nbsp;<strong>care@jarir.com</strong></a>\r\n" + 
			"											</td>\r\n" + 
			"											<td\r\n" + 
			"												style=\"width: 30%; font-family: 'Tajawal', Helvetica, Arial, sans-serif; font-size: 12.0px; text-align: center; border-left: 1px solid #8492A6;\">\r\n" + 
			"												<a href=\"https://jarir.com\"><img width=\"80%\"\r\n" + 
			"													alt=\"visit Jarir.com\"\r\n" + 
			"													src=\"http://www.jarir.com/media/wysiwyg/Saudi/email/website.png\"></a>\r\n" + 
			"												<br>\r\n" + 
			"												<p\r\n" + 
			"													style=\"color: white; font-family: 'Tajawal', Helvetica, Arial, sans-serif; font-size: 13.0px;\">\r\n" + 
			"													<strong>حـمـل الـتـطبـيـق وابـدأ التسـوق<br>\r\n" + 
			"													<span style=\"font-size: 11.0px;\">Download our app\r\n" + 
			"															&amp; start shopping</span></strong>\r\n" + 
			"												</p> <a\r\n" + 
			"												href=\"https://itunes.apple.com/us/app/jarir-bookstore-%D9%85%D9%83%D8%AA%D8%A8%D8%A9-%D8%AC%D8%B1%D9%8A%D8%B1/id535777677?mt=8\"><img\r\n" + 
			"													width=\"50%\" alt=\"appstore\"\r\n" + 
			"													src=\"http://www.jarir.com/media/wysiwyg/Saudi/email/appstore.png\"></a>\r\n" + 
			"												<br> <a\r\n" + 
			"												href=\"https://play.google.com/store/apps/details?id=com.jarirbookstore.JBMarketingApp\"><img\r\n" + 
			"													width=\"50%\" alt=\"playstore\"\r\n" + 
			"													src=\"http://www.jarir.com/media/wysiwyg/Saudi/email/playstore.png\"></a>\r\n" + 
			"												<br>\r\n" + 
			"											</td>\r\n" + 
			"											<td\r\n" + 
			"												style=\"width: 30%; font-family: 'Tajawal', Helvetica, Arial, sans-serif; font-size: 20.0px; text-align: center; border-left: 1px solid #8492A6;\">\r\n" + 
			"												<p\r\n" + 
			"													style=\"color: white; font-family: 'Tajawal', Helvetica, Arial, sans-serif; font-size: 13.0px;\">\r\n" + 
			"													<strong>تـابـعنـا لكي لا تـفـوتـك عـروضـنا<br>\r\n" + 
			"													<span style=\"font-size: 11.0px;\">Follow us to never\r\n" + 
			"															miss our offers</span></strong>\r\n" + 
			"												</p>\r\n" + 
			"												<table width=\"100%\" border=\"0\" cellspacing=\"0\"\r\n" + 
			"													cellpadding=\"0\"\r\n" + 
			"													style=\"font-family: 'Cairo', Helvetica, Arial, sans-serif; font-size: 16.0px; color: rgba(255, 255, 255, 1.0); text-align: center;\">\r\n" + 
			"													<tbody>\r\n" + 
			"														<tr>\r\n" + 
			"															<td><a\r\n" + 
			"																href=\"https://www.facebook.com/jarirbookstore\"><img\r\n" + 
			"																	width=\"70%\" alt=\"facebook\"\r\n" + 
			"																	src=\"http://www.jarir.com/media/wysiwyg/Saudi/email/facebook.png\"></a></td>\r\n" + 
			"															<td><a href=\"https://twitter.com/jarirbookstore\"><img\r\n" + 
			"																	width=\"70%\" alt=\"twitter\"\r\n" + 
			"																	src=\"http://www.jarir.com/media/wysiwyg/Saudi/email/twitter.png\"></a></td>\r\n" + 
			"															<td><a\r\n" + 
			"																href=\"https://plus.google.com/+jarirbookstore\"><img\r\n" + 
			"																	width=\"70%\" alt=\"google\"\r\n" + 
			"																	src=\"http://www.jarir.com/media/wysiwyg/Saudi/email/google.png\"></a></td>\r\n" + 
			"															<td><a\r\n" + 
			"																href=\"https://www.instagram.com/jarirbookstore\"><img\r\n" + 
			"																	width=\"70%\" alt=\"instagram\"\r\n" + 
			"																	src=\"http://www.jarir.com/media/wysiwyg/Saudi/email/instagram.png\"></a></td>\r\n" + 
			"															<td><a href=\"https://www.youtube.com/user/jarirtube\"><img\r\n" + 
			"																	width=\"70%\" alt=\"youtube\"\r\n" + 
			"																	src=\"http://www.jarir.com/media/wysiwyg/Saudi/email/youtube.png\"></a></td>\r\n" + 
			"															<td><a\r\n" + 
			"																href=\"https://www.snapchat.com/add/jarirbookstore\"><img\r\n" + 
			"																	width=\"70%\" alt=\"snapchat\"\r\n" + 
			"																	src=\"http://www.jarir.com/media/wysiwyg/Saudi/email/snapchat.png\"></a></td>\r\n" + 
			"														</tr>\r\n" + 
			"													</tbody>\r\n" + 
			"												</table>\r\n" + 
			"											</td>\r\n" + 
			"										</tr>\r\n" + 
			"									</tbody>\r\n" + 
			"								</table>\r\n" + 
			"								<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"10\">\r\n" + 
			"									<tbody>\r\n" + 
			"										<tr>\r\n" + 
			"											<td align=\"center\"><br />\r\n" + 
			"												<p>Your responses will be used in accordance with\r\n" + 
			"													Jarir.com privacy policy. Jarir.com privacy policy and\r\n" + 
			"													related information can be found on our website</p></td>\r\n" + 
			"										</tr>\r\n" + 
			"										<tr>\r\n" + 
			"											<td align=\"center\"><br /> <br> للخصوصية، يمكن الإطلاع على سياسة الخصوصية و المعلومات المتعلقة بها على موقعنا</td>\r\n" + 
			"										</tr>\r\n" + 
			"										<tr>\r\n" + 
			"											<td align=\"center\"><br> <img\r\n" + 
			"												style=\"height: 33px; width: 33px\"\r\n" + 
			"												src=\"http://www.jarir.com/media/wysiwyg/Saudi/Email_Template_Assests/JarirIcongray.png\">\r\n" + 
			"											</td>\r\n" + 
			"										</tr>\r\n" + 
			"									</tbody>\r\n" + 
			"								</table>\r\n" + 
			"							</td>\r\n" + 
			"						</tr>\r\n" + 
			"					</tbody>\r\n" + 
			"				</table>\r\n" + 
			"			</td>\r\n" + 
			"		</tr>\r\n" + 
			"	</tbody>\r\n" + 
			"</table>";
	
	private String emailAutoReply = "<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" height=\"100%\"\r\n" + 
			"	width=\"100%\">\r\n" + 
			"	<tbody>\r\n" + 
			"		<tr>\r\n" + 
			"			<td align=\"center\" valign=\"top\" style=\"padding: 20px 0 20px 0\">\r\n" + 
			"				<!-- [ header starts here] -->\r\n" + 
			"				<table bgcolor=\"#FFFFFF\" cellspacing=\"0\" cellpadding=\"3\" border=\"0\"\r\n" + 
			"					width=\"768\"\r\n" + 
			"					style=\"border: 1px solid #E0E0E0; padding: 10px; border-radius: 8px;\">\r\n" + 
			"					<tbody>\r\n" + 
			"						<tr bgcolor=\"#ffffff\">\r\n" + 
			"							<td valign=\"top\">\r\n" + 
			"								<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"\r\n" + 
			"									style=\"\">\r\n" + 
			"									<tbody>\r\n" + 
			"										<tr style=\"width: 100%; text-align: center;\">\r\n" + 
			"											<td style=\"width: 100%; padding: 5px;\" valign=\"top\"\r\n" + 
			"												align=\"center\"><img style=\"width: 738px;\"\r\n" + 
			"												src=\"http://www.jarir.com/media/wysiwyg/Saudi/email/benefitsBar.png\">\r\n" + 
			"											</td>\r\n" + 
			"										</tr>\r\n" + 
			"									</tbody>\r\n" + 
			"								</table>\r\n" + 
			"							</td>\r\n" + 
			"						</tr>\r\n" + 
			"						<tr bgcolor=\"#FFFFFF\">\r\n" + 
			"							<td valign=\"top\" align=\"center\"><img\r\n" + 
			"								src=\"http://www.jarir.com/media/wysiwyg/Saudi/Email_Template_Assests/JarirLogoTrans.png\"\r\n" + 
			"								alt=\"Heading\" width=\"370\" height=\"34\"\r\n" + 
			"								style=\"display: block; margin-top: 10px;\"></td>\r\n" + 
			"						</tr>\r\n" + 
			"						<tr bgcolor=\"#ffffff\">\r\n" + 
			"							<td valign=\"top\">\r\n" + 
			"								<table width=\"100%\" align=\"center\" border=\"0\" cellspacing=\"0\"\r\n" + 
			"									cellpadding=\"10\">\r\n" + 
			"									<tbody>\r\n" + 
			"										<tr>\r\n" + 
			"											<td width=\"30%\"\r\n" + 
			"												style=\"text-align: left; font-family: 'Tajawal', Helvetica, Arial, sans-serif; font-size: 17.0px;\"\r\n" + 
			"												align=\"center text-align: left; color:#000000\"><strong>Dear\r\n" + 
			"													Customer,</strong></td>\r\n" + 
			"											<td width=\"40%\" align=\"center\"\r\n" + 
			"												style=\"font-family: 'Tajawal', Helvetica, Arial, sans-serif; font-size: 17.0px !important;\"><strong></strong></td>\r\n" + 
			"											<td width=\"30%\" align=\"right\"\r\n" + 
			"												style=\"font-family: 'Tajawal', Helvetica, Arial, sans-serif; direction: rtl; font-size: 17.0px !important;\"><strong>عميلنا\r\n" + 
			"													العزيز،</strong></td>\r\n" + 
			"										</tr>\r\n" + 
			"									</tbody>\r\n" + 
			"								</table>\r\n" + 
			"								<table width=\"100%\" align=\"center\" border=\"0\" cellspacing=\"0\"\r\n" + 
			"									cellpadding=\"10\">\r\n" + 
			"									<tbody>\r\n" + 
			"										<tr>\r\n" + 
			"											<td width=\"40%\"\r\n" + 
			"												style=\"vertical-align:top;text-align: left; font-family: 'Tajawal', Helvetica, Arial, sans-serif; font-size: 16.0px; line-height: 26px;\"\r\n" + 
			"												align=\"center\">\r\n" + 
			"												<p>\r\n" + 
			"													Thank you for contacting Jarir Bookstore. <br /><br />\r\n" + 
			"                          Your email has been received and will be replied to within one working day. <br /><br />\r\n" + 
//			"                          For any assistance, please, do not hesitate to contact us at 920000089\r\n" + 
			"												</p>\r\n" + 
			"											</td>\r\n" + 
			"											<td width=\"40%\" align=\"right\"\r\n" + 
			"												style=\"vertical-align:top;font-family: 'Tajawal', Helvetica, Arial, sans-serif; font-size: 16.0px; text-align: right; direction: rtl; line-height: 26px;\">\r\n" + 
			"												<p>\r\n" + 
			"                          شكراً لتواصلكم مع مكتبة جرير.<br /><br />\r\n" + 
			"                          تم إستقبال بريدك الإلكتروني وسيتم الرد عليك خلال يوم عمل واحد.<br /><br /><br />\r\n" + 
//			"                          لأي استفسارات أخرى لا تتردد بالتواصل معنا عن طريق الرقم الموحد 920000089\r\n" + 
			"                        </p>\r\n" + 
			"											</td>\r\n" + 
			"										</tr>\r\n" +
			//"										<tr>\r\n" +
			//"											<td style=\"vertical-align: middle;font-family: 'Tajawal', Helvetica, Arial, sans-serif; font-size: 12px; line-height: 26px;padding: 0.01em 16px;border-left: 6px solid #2196F3;\">\r\n" +
			//"												<p>${original_email_body}</p>" +
			//"											</td>\r\n" +
			//"										</tr>\r\n" +
			"									</tbody>\r\n" + 
			"								</table>\r\n" + 
			"								<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"10\"\r\n" + 
			"									style=\"background: #3C4858; padding: 10px; border-radius: 8px;\">\r\n" + 
			"									<tbody>\r\n" + 
			"										<tr style=\"background: #3C4858\">\r\n" + 
			"											<td\r\n" + 
			"												style=\"width: 30%; font-family: 'Tajawal', Helvetica, Arial, sans-serif; font-size: 16.0px; color: rgba(255, 255, 255, 1.0); text-align: center;\">\r\n" + 
			"												<p\r\n" + 
			"													style=\"font-family: 'Tajawal', Helvetica, Arial, sans-serif; font-size: 13.0px;\">\r\n" + 
			"													<strong>تــحــتــاج مــســاعــدة؟<br>N E E\r\n" + 
			"														D&nbsp;&nbsp;&nbsp;H E L P?\r\n" + 
			"													</strong>\r\n" + 
			"												</p> <a href=\"tel:920000089\"\r\n" + 
			"												style=\"font-family: 'Tajawal', Helvetica, Arial, sans-serif; text-decoration: none; color: #FFF; font-size: 26.0px;\"><img\r\n" + 
			"													width=\"13%\" style=\"position: relative; top: 5px;\"\r\n" + 
			"													src=\"http://www.jarir.com/media/wysiwyg/Saudi/email/phone.png\">&nbsp;<strong>920000089</strong></a>\r\n" + 
			"												<br>\r\n" + 
			"											<br> <a href=\"mailto:care@jarir.com\"\r\n" + 
			"												style=\"font-family: 'Tajawal', Helvetica, Arial, sans-serif; text-decoration: none; color: #FFF; font-size: 20.0px;\"><img\r\n" + 
			"													width=\"10%\" style=\"position: relative; top: 2px;\"\r\n" + 
			"													src=\"http://www.jarir.com/media/wysiwyg/Saudi/email/email.png\">&nbsp;<strong>care@jarir.com</strong></a>\r\n" + 
			"											</td>\r\n" + 
			"											<td\r\n" + 
			"												style=\"width: 30%; font-family: 'Tajawal', Helvetica, Arial, sans-serif; font-size: 12.0px; text-align: center; border-left: 1px solid #8492A6;\">\r\n" + 
			"												<a href=\"https://jarir.com\"><img width=\"80%\"\r\n" + 
			"													alt=\"visit Jarir.com\"\r\n" + 
			"													src=\"http://www.jarir.com/media/wysiwyg/Saudi/email/website.png\"></a>\r\n" + 
			"												<br>\r\n" + 
			"												<p\r\n" + 
			"													style=\"color: white; font-family: 'Tajawal', Helvetica, Arial, sans-serif; font-size: 13.0px;\">\r\n" + 
			"													<strong>حـمـل الـتـطبـيـق وابـدأ التسـوق<br>\r\n" + 
			"													<span style=\"font-size: 11.0px;\">Download our app\r\n" + 
			"															&amp; start shopping</span></strong>\r\n" + 
			"												</p> <a\r\n" + 
			"												href=\"https://itunes.apple.com/us/app/jarir-bookstore-%D9%85%D9%83%D8%AA%D8%A8%D8%A9-%D8%AC%D8%B1%D9%8A%D8%B1/id535777677?mt=8\"><img\r\n" + 
			"													width=\"50%\" alt=\"appstore\"\r\n" + 
			"													src=\"http://www.jarir.com/media/wysiwyg/Saudi/email/appstore.png\"></a>\r\n" + 
			"												<br> <a\r\n" + 
			"												href=\"https://play.google.com/store/apps/details?id=com.jarirbookstore.JBMarketingApp\"><img\r\n" + 
			"													width=\"50%\" alt=\"playstore\"\r\n" + 
			"													src=\"http://www.jarir.com/media/wysiwyg/Saudi/email/playstore.png\"></a>\r\n" + 
			"												<br>\r\n" + 
			"											</td>\r\n" + 
			"											<td\r\n" + 
			"												style=\"width: 30%; font-family: 'Tajawal', Helvetica, Arial, sans-serif; font-size: 20.0px; text-align: center; border-left: 1px solid #8492A6;\">\r\n" + 
			"												<p\r\n" + 
			"													style=\"color: white; font-family: 'Tajawal', Helvetica, Arial, sans-serif; font-size: 13.0px;\">\r\n" + 
			"													<strong>تـابـعنـا لكي لا تـفـوتـك عـروضـنا<br>\r\n" + 
			"													<span style=\"font-size: 11.0px;\">Follow us to never\r\n" + 
			"															miss our offers</span></strong>\r\n" + 
			"												</p>\r\n" + 
			"												<table width=\"100%\" border=\"0\" cellspacing=\"0\"\r\n" + 
			"													cellpadding=\"0\"\r\n" + 
			"													style=\"font-family: 'Cairo', Helvetica, Arial, sans-serif; font-size: 16.0px; color: rgba(255, 255, 255, 1.0); text-align: center;\">\r\n" + 
			"													<tbody>\r\n" + 
			"														<tr>\r\n" + 
			"															<td><a\r\n" + 
			"																href=\"https://www.facebook.com/jarirbookstore\"><img\r\n" + 
			"																	width=\"70%\" alt=\"facebook\"\r\n" + 
			"																	src=\"http://www.jarir.com/media/wysiwyg/Saudi/email/facebook.png\"></a></td>\r\n" + 
			"															<td><a href=\"https://twitter.com/jarirbookstore\"><img\r\n" + 
			"																	width=\"70%\" alt=\"twitter\"\r\n" + 
			"																	src=\"http://www.jarir.com/media/wysiwyg/Saudi/email/twitter.png\"></a></td>\r\n" + 
			"															<td><a\r\n" + 
			"																href=\"https://plus.google.com/+jarirbookstore\"><img\r\n" + 
			"																	width=\"70%\" alt=\"google\"\r\n" + 
			"																	src=\"http://www.jarir.com/media/wysiwyg/Saudi/email/google.png\"></a></td>\r\n" + 
			"															<td><a\r\n" + 
			"																href=\"https://www.instagram.com/jarirbookstore\"><img\r\n" + 
			"																	width=\"70%\" alt=\"instagram\"\r\n" + 
			"																	src=\"http://www.jarir.com/media/wysiwyg/Saudi/email/instagram.png\"></a></td>\r\n" + 
			"															<td><a href=\"https://www.youtube.com/user/jarirtube\"><img\r\n" + 
			"																	width=\"70%\" alt=\"youtube\"\r\n" + 
			"																	src=\"http://www.jarir.com/media/wysiwyg/Saudi/email/youtube.png\"></a></td>\r\n" + 
			"															<td><a\r\n" + 
			"																href=\"https://www.snapchat.com/add/jarirbookstore\"><img\r\n" + 
			"																	width=\"70%\" alt=\"snapchat\"\r\n" + 
			"																	src=\"http://www.jarir.com/media/wysiwyg/Saudi/email/snapchat.png\"></a></td>\r\n" + 
			"														</tr>\r\n" + 
			"													</tbody>\r\n" + 
			"												</table>\r\n" + 
			"											</td>\r\n" + 
			"										</tr>\r\n" + 
			"									</tbody>\r\n" + 
			"								</table>\r\n" + 
			"								<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"10\">\r\n" + 
			"									<tbody>\r\n" + 
			"										<tr>\r\n" + 
			"											<td align=\"center\"><br />\r\n" + 
			"												<p>Your responses will be used in accordance with\r\n" + 
			"													Jarir.com privacy policy. Jarir.com privacy policy and\r\n" + 
			"													related information can be found on our website</p></td>\r\n" + 
			"										</tr>\r\n" + 
			"										<tr>\r\n" + 
			"											<td align=\"center\"><br /> <br> للخصوصية. يمكن\r\n" + 
			"												الإطلاع على سياسة الخصوصية و المعلومات المتعلقة بها على\r\n" + 
			"												موقعنا .</td>\r\n" + 
			"										</tr>\r\n" + 
			"										<tr>\r\n" + 
			"											<td align=\"center\"><br> <img\r\n" + 
			"												style=\"height: 33px; width: 33px\"\r\n" + 
			"												src=\"http://www.jarir.com/media/wysiwyg/Saudi/Email_Template_Assests/JarirIcongray.png\">\r\n" + 
			"											</td>\r\n" + 
			"										</tr>\r\n" + 
			"									</tbody>\r\n" + 
			"								</table>\r\n" + 
			"							</td>\r\n" + 
			"						</tr>\r\n" + 
			"					</tbody>\r\n" + 
			"				</table>\r\n" + 
			"			</td>\r\n" + 
			"		</tr>\r\n" + 
			"	</tbody>\r\n" + 
			"</table>";

	private String emailAutoReplyDynamic = "<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" height=\"100%\"\r\n" + 
			"	width=\"100%\">\r\n" + 
			"	<tbody>\r\n" + 
			"		<tr>\r\n" + 
			"			<td align=\"center\" valign=\"top\" style=\"padding: 20px 0 20px 0\">\r\n" + 
			"				<!-- [ header starts here] -->\r\n" + 
			"				<table bgcolor=\"#FFFFFF\" cellspacing=\"0\" cellpadding=\"3\" border=\"0\"\r\n" + 
			"					width=\"768\"\r\n" + 
			"					style=\"border: 1px solid #E0E0E0; padding: 10px; border-radius: 8px;\">\r\n" + 
			"					<tbody>\r\n" + 
			"						<tr bgcolor=\"#ffffff\">\r\n" + 
			"							<td valign=\"top\">\r\n" + 
			"								<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"\r\n" + 
			"									style=\"\">\r\n" + 
			"									<tbody>\r\n" + 
			"										<tr style=\"width: 100%; text-align: center;\">\r\n" + 
			"											<td style=\"width: 100%; padding: 5px;\" valign=\"top\"\r\n" + 
			"												align=\"center\"><img style=\"width: 738px;\"\r\n" + 
			"												src=\"http://www.jarir.com/media/wysiwyg/Saudi/email/benefitsBar.png\">\r\n" + 
			"											</td>\r\n" + 
			"										</tr>\r\n" + 
			"									</tbody>\r\n" + 
			"								</table>\r\n" + 
			"							</td>\r\n" + 
			"						</tr>\r\n" + 
			"						<tr bgcolor=\"#FFFFFF\">\r\n" + 
			"							<td valign=\"top\" align=\"center\"><img\r\n" + 
			"								src=\"http://www.jarir.com/media/wysiwyg/Saudi/Email_Template_Assests/JarirLogoTrans.png\"\r\n" + 
			"								alt=\"Heading\" width=\"370\" height=\"34\"\r\n" + 
			"								style=\"display: block; margin-top: 10px;\"></td>\r\n" + 
			"						</tr>\r\n" + 
			"						<tr bgcolor=\"#ffffff\">\r\n" + 
			"							<td valign=\"top\">\r\n" + 
			"								<table width=\"100%\" align=\"center\" border=\"0\" cellspacing=\"0\"\r\n" + 
			"									cellpadding=\"10\">\r\n" + 
			"									<tbody>\r\n" + 
			"										<tr>\r\n" + 
			"											<td width=\"30%\"\r\n" + 
			"												style=\"text-align: left; font-family: 'Tajawal', Helvetica, Arial, sans-serif; font-size: 17.0px;\"\r\n" + 
			"												align=\"center text-align: left; color:#000000\"><strong>Dear\r\n" + 
			"													Customer,</strong></td>\r\n" + 
			"											<td width=\"40%\" align=\"center\"\r\n" + 
			"												style=\"font-family: 'Tajawal', Helvetica, Arial, sans-serif; font-size: 17.0px !important;\"><strong></strong></td>\r\n" + 
			"											<td width=\"30%\" align=\"right\"\r\n" + 
			"												style=\"font-family: 'Tajawal', Helvetica, Arial, sans-serif; direction: rtl; font-size: 17.0px !important;\"><strong>عميلنا\r\n" + 
			"													العزيز،</strong></td>\r\n" + 
			"										</tr>\r\n" + 
			"									</tbody>\r\n" + 
			"								</table>\r\n" + 
			"								<table width=\"100%\" align=\"center\" border=\"0\" cellspacing=\"0\"\r\n" + 
			"									cellpadding=\"10\">\r\n" + 
			"									<tbody>\r\n" + 
			"										<tr>\r\n" + 
			"											<td width=\"40%\"\r\n" + 
			"												style=\"vertical-align:top;text-align: left; font-family: 'Tajawal', Helvetica, Arial, sans-serif; font-size: 16.0px; line-height: 26px;\"\r\n" + 
			"												align=\"center\">\r\n" + 
			"												<p>\r\n" +
			"												${auto-reply-eng-body}\r\n" +
			"												</p>\r\n" + 
			"											</td>\r\n" + 
			"											<td width=\"40%\" align=\"right\"\r\n" + 
			"												style=\"vertical-align:top;font-family: 'Tajawal', Helvetica, Arial, sans-serif; font-size: 16.0px; text-align: right; direction: rtl; line-height: 26px;\">\r\n" + 
			"												<p>\r\n" +
			"												${auto-reply-ara-body}\r\n" +
			"												</p>\r\n" +  
			"											</td>\r\n" + 
			"										</tr>\r\n" +
			"									</tbody>\r\n" + 
			"								</table>\r\n" + 
			"								<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"10\"\r\n" + 
			"									style=\"background: #3C4858; padding: 10px; border-radius: 8px;\">\r\n" + 
			"									<tbody>\r\n" + 
			"										<tr style=\"background: #3C4858\">\r\n" + 
			"											<td\r\n" + 
			"												style=\"width: 30%; font-family: 'Tajawal', Helvetica, Arial, sans-serif; font-size: 16.0px; color: rgba(255, 255, 255, 1.0); text-align: center;\">\r\n" + 
			"												<p\r\n" + 
			"													style=\"font-family: 'Tajawal', Helvetica, Arial, sans-serif; font-size: 13.0px;\">\r\n" + 
			"													<strong>تــحــتــاج مــســاعــدة؟<br>N E E\r\n" + 
			"														D&nbsp;&nbsp;&nbsp;H E L P?\r\n" + 
			"													</strong>\r\n" + 
			"												</p> <a href=\"tel:920000089\"\r\n" + 
			"												style=\"font-family: 'Tajawal', Helvetica, Arial, sans-serif; text-decoration: none; color: #FFF; font-size: 26.0px;\"><img\r\n" + 
			"													width=\"13%\" style=\"position: relative; top: 5px;\"\r\n" + 
			"													src=\"http://www.jarir.com/media/wysiwyg/Saudi/email/phone.png\">&nbsp;<strong>920000089</strong></a>\r\n" + 
			"												<br>\r\n" + 
			"											<br> <a href=\"mailto:care@jarir.com\"\r\n" + 
			"												style=\"font-family: 'Tajawal', Helvetica, Arial, sans-serif; text-decoration: none; color: #FFF; font-size: 20.0px;\"><img\r\n" + 
			"													width=\"10%\" style=\"position: relative; top: 2px;\"\r\n" + 
			"													src=\"http://www.jarir.com/media/wysiwyg/Saudi/email/email.png\">&nbsp;<strong>care@jarir.com</strong></a>\r\n" + 
			"											</td>\r\n" + 
			"											<td\r\n" + 
			"												style=\"width: 30%; font-family: 'Tajawal', Helvetica, Arial, sans-serif; font-size: 12.0px; text-align: center; border-left: 1px solid #8492A6;\">\r\n" + 
			"												<a href=\"https://jarir.com\"><img width=\"80%\"\r\n" + 
			"													alt=\"visit Jarir.com\"\r\n" + 
			"													src=\"http://www.jarir.com/media/wysiwyg/Saudi/email/website.png\"></a>\r\n" + 
			"												<br>\r\n" + 
			"												<p\r\n" + 
			"													style=\"color: white; font-family: 'Tajawal', Helvetica, Arial, sans-serif; font-size: 13.0px;\">\r\n" + 
			"													<strong>حـمـل الـتـطبـيـق وابـدأ التسـوق<br>\r\n" + 
			"													<span style=\"font-size: 11.0px;\">Download our app\r\n" + 
			"															&amp; start shopping</span></strong>\r\n" + 
			"												</p> <a\r\n" + 
			"												href=\"https://itunes.apple.com/us/app/jarir-bookstore-%D9%85%D9%83%D8%AA%D8%A8%D8%A9-%D8%AC%D8%B1%D9%8A%D8%B1/id535777677?mt=8\"><img\r\n" + 
			"													width=\"50%\" alt=\"appstore\"\r\n" + 
			"													src=\"http://www.jarir.com/media/wysiwyg/Saudi/email/appstore.png\"></a>\r\n" + 
			"												<br> <a\r\n" + 
			"												href=\"https://play.google.com/store/apps/details?id=com.jarirbookstore.JBMarketingApp\"><img\r\n" + 
			"													width=\"50%\" alt=\"playstore\"\r\n" + 
			"													src=\"http://www.jarir.com/media/wysiwyg/Saudi/email/playstore.png\"></a>\r\n" + 
			"												<br>\r\n" + 
			"											</td>\r\n" + 
			"											<td\r\n" + 
			"												style=\"width: 30%; font-family: 'Tajawal', Helvetica, Arial, sans-serif; font-size: 20.0px; text-align: center; border-left: 1px solid #8492A6;\">\r\n" + 
			"												<p\r\n" + 
			"													style=\"color: white; font-family: 'Tajawal', Helvetica, Arial, sans-serif; font-size: 13.0px;\">\r\n" + 
			"													<strong>تـابـعنـا لكي لا تـفـوتـك عـروضـنا<br>\r\n" + 
			"													<span style=\"font-size: 11.0px;\">Follow us to never\r\n" + 
			"															miss our offers</span></strong>\r\n" + 
			"												</p>\r\n" + 
			"												<table width=\"100%\" border=\"0\" cellspacing=\"0\"\r\n" + 
			"													cellpadding=\"0\"\r\n" + 
			"													style=\"font-family: 'Cairo', Helvetica, Arial, sans-serif; font-size: 16.0px; color: rgba(255, 255, 255, 1.0); text-align: center;\">\r\n" + 
			"													<tbody>\r\n" + 
			"														<tr>\r\n" + 
			"															<td><a\r\n" + 
			"																href=\"https://www.facebook.com/jarirbookstore\"><img\r\n" + 
			"																	width=\"70%\" alt=\"facebook\"\r\n" + 
			"																	src=\"http://www.jarir.com/media/wysiwyg/Saudi/email/facebook.png\"></a></td>\r\n" + 
			"															<td><a href=\"https://twitter.com/jarirbookstore\"><img\r\n" + 
			"																	width=\"70%\" alt=\"twitter\"\r\n" + 
			"																	src=\"http://www.jarir.com/media/wysiwyg/Saudi/email/twitter.png\"></a></td>\r\n" + 
			"															<td><a\r\n" + 
			"																href=\"https://plus.google.com/+jarirbookstore\"><img\r\n" + 
			"																	width=\"70%\" alt=\"google\"\r\n" + 
			"																	src=\"http://www.jarir.com/media/wysiwyg/Saudi/email/google.png\"></a></td>\r\n" + 
			"															<td><a\r\n" + 
			"																href=\"https://www.instagram.com/jarirbookstore\"><img\r\n" + 
			"																	width=\"70%\" alt=\"instagram\"\r\n" + 
			"																	src=\"http://www.jarir.com/media/wysiwyg/Saudi/email/instagram.png\"></a></td>\r\n" + 
			"															<td><a href=\"https://www.youtube.com/user/jarirtube\"><img\r\n" + 
			"																	width=\"70%\" alt=\"youtube\"\r\n" + 
			"																	src=\"http://www.jarir.com/media/wysiwyg/Saudi/email/youtube.png\"></a></td>\r\n" + 
			"															<td><a\r\n" + 
			"																href=\"https://www.snapchat.com/add/jarirbookstore\"><img\r\n" + 
			"																	width=\"70%\" alt=\"snapchat\"\r\n" + 
			"																	src=\"http://www.jarir.com/media/wysiwyg/Saudi/email/snapchat.png\"></a></td>\r\n" + 
			"														</tr>\r\n" + 
			"													</tbody>\r\n" + 
			"												</table>\r\n" + 
			"											</td>\r\n" + 
			"										</tr>\r\n" + 
			"									</tbody>\r\n" + 
			"								</table>\r\n" + 
			"								<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"10\">\r\n" + 
			"									<tbody>\r\n" + 
			"										<tr>\r\n" + 
			"											<td align=\"center\"><br />\r\n" + 
			"												<p>Your responses will be used in accordance with\r\n" + 
			"													Jarir.com privacy policy. Jarir.com privacy policy and\r\n" + 
			"													related information can be found on our website</p></td>\r\n" + 
			"										</tr>\r\n" + 
			"										<tr>\r\n" + 
			"											<td align=\"center\"><br /> <br> للخصوصية. يمكن\r\n" + 
			"												الإطلاع على سياسة الخصوصية و المعلومات المتعلقة بها على\r\n" + 
			"												موقعنا .</td>\r\n" + 
			"										</tr>\r\n" + 
			"										<tr>\r\n" + 
			"											<td align=\"center\"><br> <img\r\n" + 
			"												style=\"height: 33px; width: 33px\"\r\n" + 
			"												src=\"http://www.jarir.com/media/wysiwyg/Saudi/Email_Template_Assests/JarirIcongray.png\">\r\n" + 
			"											</td>\r\n" + 
			"										</tr>\r\n" + 
			"									</tbody>\r\n" + 
			"								</table>\r\n" + 
			"							</td>\r\n" + 
			"						</tr>\r\n" + 
			"					</tbody>\r\n" + 
			"				</table>\r\n" + 
			"			</td>\r\n" + 
			"		</tr>\r\n" + 
			"	</tbody>\r\n" + 
			"</table>";
	
	public String getEmailTemplateStart() {
		return emailTemplateStart;
	}

	public String getEmailTemplateEnd() {
		return emailTemplateEnd;
	}

	public String getEmailAutoReply() {
		return emailAutoReply;
	}
	
	public String getEmailAutoReplyDynamic() {
		return emailAutoReplyDynamic;
	}
}
