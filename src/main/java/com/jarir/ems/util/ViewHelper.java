package com.jarir.ems.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import com.jarir.ems.repos.ems.JarirEmailRepository;
import com.jarir.ems.repos.ems.TicketRepository;

@Service
public class ViewHelper {
	@Autowired
	JarirEmailRepository emailRepository;
	
	@Autowired
	TicketRepository ticketRepository;
	
	public Model updateCounts(Model model) {
		model.addAttribute("pendingCount", emailRepository.findByStatus("Pending").size());
//		model.addAttribute("noactionCount", emailRepository.findByStatus("NoAction").size());
		model.addAttribute("noactionCount", emailRepository.countByStatus("NoAction"));
		long total = emailRepository.countByStatus("Replied") + emailRepository.countByStatus("Replied and Forwarded");
//		model.addAttribute("followupCount", emailRepository.findByStatus("FollowUp").size());
//		model.addAttribute("closedCount", emailRepository.findByStatusContains("Replied").size());
		model.addAttribute("closedCount", total);
		
//		model.addAttribute("pendingTicketsCount", ticketRepository.findAllByStatus("Pending").size());
//		model.addAttribute("closedTicketsCount", ticketRepository.findAllByStatus("Closed").size());
		return model;
	} 
}
