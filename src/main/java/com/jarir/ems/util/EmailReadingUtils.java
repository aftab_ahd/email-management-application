package com.jarir.ems.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.mail.Address;
import javax.mail.BodyPart;
import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.Transport;
import javax.mail.UIDFolder;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.jsoup.Jsoup;
import org.jsoup.examples.HtmlToPlainText;
import org.springframework.stereotype.Service;

import com.jarir.ems.entities.ems.EmailAccount;
import com.jarir.ems.entities.ems.EmailAttachment;
import com.jarir.ems.entities.ems.EmailTemplateParameter;
import com.jarir.ems.entities.ems.JarirEmail;
import com.sun.mail.util.BASE64DecoderStream;
import com.sun.mail.util.QPDecoderStream;

@Service
public class EmailReadingUtils {

	@SuppressWarnings("unused")
	private void replyToMessage(Message message) throws AddressException, MessagingException {
		Properties properties = new Properties();
		properties.put("mail.smtp.auth", "true");
		properties.put("mail.smtp.starttls.enable", "true");
		properties.put("mail.smtp.host", "jbemail01.jarirbookstore.com");
		properties.put("mail.smtp.port", "25");

		Session session = Session.getInstance(properties);
//		Store store = emailSession.getStore("imaps");
//		Message replyMessage = new MimeMessage(session);
		MimeMessage replyMessage = new MimeMessage(session);
		replyMessage = (MimeMessage) message.reply(false);
		replyMessage.setFrom(new InternetAddress("care-test@jarir.com"));
		
//		replyMessage.setText("Thanks");
		String text = "اختبار" ;
		replyMessage.setText(text, "utf-8", "html");
		
//		System.out.println("ReplyTO:");
		for(Address address: message.getReplyTo())
//			System.out.println(address);

		System.out.println("All:");
		for(Address address: replyMessage.getAllRecipients())
			System.out.println(address);
		
//		replyMessage.setReplyTo(message.getReplyTo());

		// Send the message by authenticating the SMTP server
		// Create a Transport instance and call the sendMessage
		Transport t = session.getTransport("smtp");
//		session.h
		try {
			// connect to the smpt server using transport instance
			// change the user and password accordingly
			t.connect("care-test@jarir.com", "Jarir123");
//			t.sendMessage(replyMessage, replyMessage.getAllRecipients());
			t.sendMessage(replyMessage, new Address[] {new InternetAddress("hitham.alqadheeb@gmail.com")});
		} finally {
			t.close();
		}
		System.out.println("message replied successfully ....");
	}
	
//	public List<JarirEmail> getAllEmails(List<EmailAccount> accounts, Folder folder) throws MessagingException {
//		List<JarirEmail> jarirEmails = new ArrayList<>();
////		for(EmailAccount account : accounts) {
////			Folder folder = readEmailFolder(account.getHost(), account.getUsername(), account.getUserpassword());
//			UIDFolder uf = (UIDFolder) folder;
//			Message[] messages = folder.getMessages();
//			
////			int counter = 1;
//			for (int i = 0; i < messages.length; i++) {
//				Long emailUid = uf.getUID(messages[i]);
//				
//				//System.out.println("Counter:"+counter+" "+messages[i].getFrom()[0].toString().toLowerCase().contains("hitham"));
//				//if(counter == 1 && messages[i].getFrom()[0].toString().toLowerCase().contains("hitham")) {
//				//	++counter;
//				//	replyToMessage(messages[i]);
//				//}
//				
//				try {
//					jarirEmails.add(parseMessage(messages[i], emailUid, account));
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//			}
////		}
//		jarirEmails.sort((o1, o2) -> o1.getSentDate().compareTo(o2.getSentDate()));
//		return jarirEmails; 
//	}
	
	
	
	public boolean isProbablyArabic(String s) {
		for (int i = 0; i < s.length();) {
			int c = s.codePointAt(i);
			if (c >= 0x0600 && c <= 0x06E0)
				return true;
			i += Character.charCount(c);
		}
		return false;
	}
	
	public String includeParameters(String body, List<EmailTemplateParameter> params/*, String lang*/) {
		String result = body;
		for(EmailTemplateParameter param : params) {
			result = result.replaceAll(param.getName(), param.getValue());
			//if(lang.equals("Arabic")) 
				//result = body.replaceAll(param.getNameArabic(), param.getValue());
			//else if(lang.equals("English"))
				//result = body.replaceAll(param.getNameEnglish(), param.getValue());
		}
		return result;
	}
	
	/*
	 * 
	 * 
	 * Steps:
	 * 		1. Get All Original Emails (No In-Reply-To)
	 * 		2. Save if not already saved (? or just keep reading from Email Server ?)
	 * 		3. Get All Emails with In-Reply-To
	 * 		4. Build Tree between Original and Reply-To emails together
	 * 		5. Each Email will have files associated
	 * 		6. Each Email will have Action (Closed/Follow-up Required, Action Date, Resolution Type) (Can be a set of actions if email is resolved twice (modify) if we make the system allow it)
	 * 
	 *  
	 */
//	public List<JarirEmail> getAllEmails() throws MessagingException {
//		String host = "jbemail01.jarirbookstore.com";
//		String username = "care-test@jarir.com";
//		String password = "Jarir123";
//		Folder folder = readEmailFolder(host, username, password);
//		UIDFolder uf = (UIDFolder) folder;
//		Message[] messages = folder.getMessages();
//		List<JarirEmail> jarirEmails = new ArrayList<>();
//		for (int i = 0; i < messages.length; i++) {
//			Long emailUid = uf.getUID(messages[i]);
//			try {
//				jarirEmails.add(parseMessage(messages[i], emailUid));
//			} catch (Exception e) {
//				e.printStackTrace();
//			}
//		}
//		return jarirEmails;
//	}
	
	private static Pattern allowedFilesPtrn = Pattern.compile("([^\\s]+(\\.(?i)(txt|doc|csv|pdf|png|jpeg|jpg))$)");
	private EmailAttachment saveEmailAttachment(EmailAttachment attachment, Long accountId, JarirEmail email) {
		//System.out.println("Saving attachment:");
		//System.out.println("AccountID:"+accountId);
		//System.out.println("getEmailUid:"+email.getEmailUid());
		//System.out.println("getFilename:"+attachment.getFilename());
		
		File file = new File("/home/ems/emailattachments/" + accountId +"/"+ email.getEmailUid() + "/" + attachment.getFilename() );
		FilterInputStream in = null;
		if(attachment.getInputstream() instanceof QPDecoderStream)
		{
			in = (QPDecoderStream) attachment.getInputstream();
		}
		else
		{
			in = (BASE64DecoderStream) attachment.getInputstream();
		}
		
		FileOutputStream out = null;
		try {
			if(!file.exists())
				file.getParentFile().mkdirs();
			
			out = new FileOutputStream(file);
			int buf;
			while((buf = in.read()) != -1) {
			     out.write(buf);
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try { if(out!=null) out.close(); } catch (IOException e) { }
			try { if(in!=null) in.close(); } catch (IOException e) { }
		}
		String filePath = file.getAbsolutePath().replaceAll("D:", "");
		filePath = filePath.replaceAll("\\\\", "/");
//		attachment.setFilepath(file.getAbsolutePath());
//		System.out.println("filePath="+filePath);
		attachment.setFilepath(filePath);
		attachment.setJarirEmail(email);
		return attachment;

	}

	private JarirEmail parseMessage(Message message, Long emailUid, EmailAccount account) throws Exception {

		JarirEmail email = new JarirEmail();
		email.setFromEmail(message.getFrom()[0].toString());
		email.setSubject(message.getSubject());
		email.setEmailUid(emailUid);
		email.setToEmail(getRecipientsStringFromMessage(message));
		email.setCcEmail(getCcStringFromMessage(message));
		email.setSentDate(message.getSentDate());
		email.setReceivedDate(message.getReceivedDate());
		email.setBody(getTextFromMessage(message, false));
//		email.setBodyHtml(getTextFromMessage(message, true));
		email.setInReplyTo(getHeaderValue(message, "In-Reply-To"));
		email.setMessageId(getHeaderValue(message, "Message-ID"));
		email.setReferencesEmails(getHeaderValue(message, "References"));
		email.setEmailAccount(account);
		email.setReplyTo(message.getReplyTo()[0].toString());

//		if(email.getBody().trim().isEmpty()) {
//			email.setBody(new HtmlToPlainText().getPlainText(Jsoup.parse(email.getBodyHtml())));
//		}
		
		//if(message.getReplyTo().length>0)
		//	System.out.println("===> Reply-TO:"+message.getReplyTo()[0].toString());
		//else
		//	System.out.println("===> Reply-TO is Empty");
		
		List<EmailAttachment> attachments = getAttachments(message);
//		System.out.println(attachments.size());
		if(attachments!=null && attachments.size() > 0) {
			for(EmailAttachment attachment : attachments) {
//				System.out.println("attachment.getFilename()="+attachment.getFilename());
				if(attachment.getFilename() != null)
				{
					Matcher mtch = allowedFilesPtrn.matcher(attachment.getFilename());
			        if(mtch.matches()){
			        	email.addToEmailAttachments(saveEmailAttachment(attachment, account.getId(), email));
			        }
				}
				
			}
		}
		
//			System.out.println("All Headers are:");
//			Enumeration<Header> headers = message.getAllHeaders();
//			while(headers.hasMoreElements()) {
//				Header header = headers.nextElement();
//				System.out.println(header.getName()+":"+header.getValue());
//			}
		
		return email;
	}
	
	private String getHeaderValue(Message message, String headerName) throws MessagingException {
		String result = "";
		if(message.getHeader(headerName) != null) {
			result = message.getHeader(headerName)[0];
		}
		return result;
	}

	private String getRecipientsStringFromMessage(Message m) throws MessagingException {
		String output = "";
		String prefix = "";
		if(m != null)
		{
			if(m != null && m.getRecipients(Message.RecipientType.TO) != null)
			{
				for (Address address : m.getRecipients(Message.RecipientType.TO)) {
					output += (prefix + address.toString());
					prefix = ",";
				}
			}
			
		}
		
		return output;
	}
	
	private String getCcStringFromMessage(Message m) throws MessagingException {
		String output = "";
		String prefix = "";
		
		Address[] ccList = m.getRecipients(Message.RecipientType.CC);
		if(ccList != null) {
			for (Address address : ccList) {
				output += (prefix + address.toString());
				prefix = ",";
			}
		}
		
//		ccList = m.getRecipients(Message.RecipientType.BCC);
//		if(ccList != null) {
//			for (Address address : ccList) {
//				
//			}
//		}
		return output;
	}

	public Folder readEmailFolder(String host, String user, String password) throws MessagingException {
		Properties properties = new Properties();
		Session emailSession = Session.getDefaultInstance(properties);
		Store store = emailSession.getStore("imaps");
		store.connect(host, user, password);
		Folder folder = store.getFolder("INBOX");
		folder.open(Folder.READ_ONLY);
		return folder;
	}
	
	public void creatEmailFolders(EmailAccount account) throws MessagingException {
		Properties properties = new Properties();
		Session emailSession = Session.getDefaultInstance(properties);
		Store store = emailSession.getStore("imaps");
//		System.out.println("account.getHost()="+account.getHost()+" account.getUsername()="+account.getUsername());
		store.connect(account.getHost(), account.getUsername(), account.getUserpassword());
		
		Folder actionsFolder = store.getFolder("EMS_ACTIONS");
		if(!actionsFolder.exists()) {
			actionsFolder.create(Folder.HOLDS_MESSAGES);
		}
		
		Folder noActionsFolder = store.getFolder("EMS_NO_ACTIONS");
		if(!noActionsFolder.exists()) {
			noActionsFolder.create(Folder.HOLDS_MESSAGES);
		}
	}
	
	public void moveEmail(JarirEmail email, String folderName) throws MessagingException {
		EmailAccount account = email.getEmailAccount();
		Properties properties = new Properties();
		Session emailSession = Session.getDefaultInstance(properties);
		Store store = emailSession.getStore("imaps");
		store.connect(account.getHost(), account.getUsername(), account.getUserpassword());
		
		Folder inboxFolder = store.getFolder("INBOX");
		inboxFolder.open(Folder.READ_WRITE);
		UIDFolder uf = (UIDFolder) inboxFolder;
		Message message = uf.getMessageByUID(email.getEmailUid());
		Message[] messages = {message};
		
		// Copy
		Folder destFolder = store.getFolder(folderName);
		destFolder.open(Folder.READ_WRITE);
		inboxFolder.copyMessages(messages, destFolder);

		// Now the delete the messages from Current Folder
		messages[0].setFlag(Flags.Flag.DELETED, true);
		inboxFolder.expunge(); // causing the error javax.mail.MessageRemovedException
	}
	
	private String getTextFromMessage(Message message, boolean html) throws IOException {
	    String result = "";
	    try
	    {
	    	if(message != null)
		    {
		    	if (message.isMimeType("text/plain")) {
			        result = message.getContent().toString();
			    } else if (message.isMimeType("multipart/*")) {
			        MimeMultipart mimeMultipart = (MimeMultipart) message.getContent();
			        if(html)
			        	result = getHtmlFromMimeMultipart(mimeMultipart);
			        else
			        	result = getTextFromMimeMultipart(mimeMultipart);
			    }
		    }
	    }catch(MessagingException e)
	    {
	    	System.out.println(e);
	    }
	    
	    
	    return result;
	}

	private String getTextFromMimeMultipart(
	        MimeMultipart mimeMultipart)  throws MessagingException, IOException{
	    String result = "";
	    int count = mimeMultipart.getCount();
	    for (int i = 0; i < count; i++) {
	        BodyPart bodyPart = mimeMultipart.getBodyPart(i);
	        if (bodyPart.isMimeType("text/plain")) {
	        	try
	        	{
	        		result = result + "\n" + bodyPart.getContent();
	        	}catch(Exception e)
	        	{
	        		result = "";
	        	}
	            
	            break;
	        } else if (bodyPart.getContent() instanceof MimeMultipart){
	            result = result + getTextFromMimeMultipart((MimeMultipart)bodyPart.getContent());
	        }
	    }
	    return result;
//	    return result.replace("\n", "").replace("\r", "");
	}
	
	private String getHtmlFromMimeMultipart(
	        MimeMultipart mimeMultipart)  throws MessagingException, IOException{
	    String result = "";
	    int count = mimeMultipart.getCount();
	    for (int i = 0; i < count; i++) {
	        BodyPart bodyPart = mimeMultipart.getBodyPart(i);
	        if (bodyPart.isMimeType("text/html")) {
	            String html = (String) bodyPart.getContent();
//	            result = result + "\n" + org.jsoup.Jsoup.parse(html).text();
	            result = result + "\n" + html;
	        } else if (bodyPart.getContent() instanceof MimeMultipart){
	            result = result + getHtmlFromMimeMultipart((MimeMultipart)bodyPart.getContent());
	        }
	    }
	    return result;
//	    return result.replace("\n", "").replace("\r", "");
	}
	
	public List<EmailAttachment> getAttachments(Message message) throws Exception {
		Object content = null;
		try
		{
			content = message.getContent();
		} catch (Exception e) {
			content = "Junk";
			/*
			 * if (message instanceof MimeMessage &&
			 * "Unable to load BODYSTRUCTURE".equalsIgnoreCase(e.getMessage())) { content =
			 * ((MimeMessage) message).getContent(); } else { throw e; }
			 */
        }
	    
	    if (content instanceof String)
	        return null;        

	    if (content instanceof Multipart) {
	        Multipart multipart = (Multipart) content;
	        List<EmailAttachment> result = new ArrayList<EmailAttachment>();

	        for (int i = 0; i < multipart.getCount(); i++) {
	            result.addAll(getAttachments(multipart.getBodyPart(i)));
	        }
	        return result;

	    }
	    return null;
	}

	private List<EmailAttachment> getAttachments(BodyPart part) throws Exception {
	    List<EmailAttachment> result = new ArrayList<EmailAttachment>();
	    Object content = null;
	    try
	    {
	    	content = part.getContent();
	    }catch(Exception e)
	    {
	    	content = "";
	    }
	    
	    if (content instanceof InputStream || content instanceof String || part instanceof MimeBodyPart ) {
//	    	System.out.println("part.getDisposition()="+part.getDisposition());
	        if (Part.ATTACHMENT.equalsIgnoreCase(part.getDisposition()) || Part.INLINE.equalsIgnoreCase(part.getDisposition()) || (part!=null && part.getFileName() != null && !part.getFileName().isEmpty())) {
	        	EmailAttachment attachment = new EmailAttachment();
	        	attachment.setFilename(part.getFileName());
	        	attachment.setInputstream(part.getInputStream());
//	            result.add(part.getInputStream());
	        	result.add(attachment);
	            return result;
	        } else {
	            return new ArrayList<EmailAttachment>();
	        }
	    }

	    if (content instanceof Multipart) {
	            Multipart multipart = (Multipart) content;
	            for (int i = 0; i < multipart.getCount(); i++) {
	                BodyPart bodyPart = multipart.getBodyPart(i);
	                result.addAll(getAttachments(bodyPart));
	            }
	    }
	    return result;
	}

	public List<JarirEmail> parseMessages(Folder folder, EmailAccount account) {
		List<JarirEmail> jarirEmails = new ArrayList<>();
		UIDFolder uf = (UIDFolder) folder;
		Message[] messages;
		try {
			messages = folder.getMessages();
			for (int i = 0; i < messages.length; i++) {
				Long emailUid = uf.getUID(messages[i]);
				try {
					jarirEmails.add(parseMessage(messages[i], emailUid, account));
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		} catch (MessagingException e1) {
			e1.printStackTrace();
		}
		return jarirEmails;
	}
}
