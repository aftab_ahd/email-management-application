package com.jarir.ems;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.context.MessageSourceAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

//WAS
/*
 * @SpringBootApplication(exclude = MessageSourceAutoConfiguration.class)
 * 
 * @ServletComponentScan public class SpringBootWebApplication extends
 * SpringBootServletInitializer {
 * 
 * @Override protected SpringApplicationBuilder
 * configure(SpringApplicationBuilder application) { return
 * application.sources(SpringBootWebApplication.class); }
 * 
 * @Bean(initMethod="init") public InitEmailFolders exBean() { return new
 * InitEmailFolders(); }
 * 
 * }
 */

//BOOT
@SpringBootApplication
@EnableAutoConfiguration
public class SpringBootWebApplication {
	public static void main(String[] args) {
		SpringApplication.run(SpringBootWebApplication.class, args);
	}
	
	@Bean(initMethod="init")
	public InitEmailFolders exBean() {
	    return new InitEmailFolders();
	}
}
