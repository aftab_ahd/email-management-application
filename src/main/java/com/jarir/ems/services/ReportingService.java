package com.jarir.ems.services;

import java.io.IOException;
import java.io.OutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Year;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jarir.ems.entities.ems.EmailAccount;
import com.jarir.ems.entities.ems.EmailAction;
import com.jarir.ems.entities.ems.EmailResolution;
import com.jarir.ems.entities.ems.JarirEmail;
import com.jarir.ems.repos.ems.EmailAccountRepository;
import com.jarir.ems.repos.ems.EmailActionRepository;
import com.jarir.ems.repos.ems.EmailResolutionRepository;
import com.jarir.ems.repos.ems.JarirEmailRepository;

@Service
public class ReportingService {

	@Autowired
	JarirEmailRepository emailRepo;

	@Autowired
	EmailActionRepository actionRepo;

	@Autowired
	EmailResolutionRepository emailResolutionRepo;

	@Autowired
	EmailAccountRepository emailAccountRepo;

	private Map<String, CellStyle> styles;
	
	public List<JarirEmail> getTotalReceivedEmailsForMonth(int month) {
		int year = Calendar.getInstance().get(Calendar.YEAR);
		return emailRepo.getAllEmailsOfMonthAndYear(month, year);
	}

	public List<JarirEmail> getTotalReceivedEmailsForCurrentMonth() {
		return emailRepo.getAllEmailsOfCurrentMonth();
	}

	public List<EmailAction> getTotalActionsForCurrentMonth() {
		return actionRepo.getAllActionsOfCurrentMonth();
	}

	public List<JarirEmail> getTotalReceivedEmailsForCurrentWeek() {
		return emailRepo.getAllEmailsOfCurrentWeek();
	}

	public List<EmailAction> getTotalActionsForCurrentWeek() {
		return actionRepo.getAllActionsOfCurrentWeek();
	}

	public List<JarirEmail> getTotalReceivedEmailsForCurrentToday() {
		return emailRepo.getAllEmailsOfToday();
	}

	public List<EmailAction> getTotalActionsForCurrentToday() {
		return actionRepo.getAllActionsOfToday();
	}

	public List<EmailAction> getActionsByResolution(EmailResolution emailresolution) {
		return actionRepo.findAllByEmailResolution(emailresolution);
	}

	static int LastDayOfMonth(int year, int month) {
		return LocalDate.of(year, month, 1).getMonth().length(Year.of(year).isLeap());
	}

	public int[] allYearEmailCountPerMonth() {
		int[] totals = new int[12];
		for (int i = 1; i < 13; i++) {
			totals[i - 1] = getTotalReceivedEmailsForMonth(i).size();
		}
		return totals;
	}

	public String[] resolutionsAndCount() {
		List<EmailResolution> resolutions = emailResolutionRepo.findAll();
		String[] labelCount = new String[resolutions.size()];
		int index = 0;
		for (EmailResolution resolution : resolutions) {
			labelCount[index] = resolution.getName() + ":" + getActionsByResolution(resolution).size();
			index++;
		}
		return labelCount;
	}

	private List<EmailResolution> buildHeader() {
		return emailResolutionRepo.findAll();
//		List<String> resList = allResolutions.stream()
//                .map(EmailResolution::getName)
//                .collect(Collectors.toList());
//		return resList;
	}

	private List<String> datesMonthly(int month, int year) {
		List<String> dates = new ArrayList<>();
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.MONTH, month - 1);
		cal.set(Calendar.YEAR, year);
		cal.set(Calendar.DAY_OF_MONTH, 1);
		int maxDay = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
		SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
		dates.add(df.format(cal.getTime()));
		for (int i = 1; i < maxDay; i++) {
			cal.set(Calendar.DAY_OF_MONTH, i + 1);
			dates.add(df.format(cal.getTime()));
		}
		return dates;
	}

	private List<String> datesAnnual(int year) {
		List<String> dates = new ArrayList<>();
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.MONTH, 0);
		cal.set(Calendar.YEAR, year);
		SimpleDateFormat df = new SimpleDateFormat("MMM-yyyy");
		dates.add(df.format(cal.getTime()));
		for (int i = 1; i < 12; i++) {
			cal.set(Calendar.MONTH, i);
			dates.add(df.format(cal.getTime()));
		}
		return dates;
	}

	// dd-MMM-yyyy
	private int getDay(String date) {
		Date holder = null;
		try {
			holder = new SimpleDateFormat("dd-MMM-yyyy").parse(date);
		} catch (ParseException e1) {
			e1.printStackTrace();
		}
		Calendar cal = Calendar.getInstance();
		cal.setTime(holder);
		int day = cal.get(Calendar.DAY_OF_MONTH);
		return day;
	}

	// MMM-yyyy
	private int getMonth(String date) {
		Date holder = null;
		try {
			holder = new SimpleDateFormat("MMM-yyyy").parse(date);
		} catch (ParseException e1) {
			e1.printStackTrace();
		}
		Calendar cal = Calendar.getInstance();
		cal.setTime(holder);
		int month = cal.get(Calendar.MONTH);
		return month;
	}

	public void generateExcelReport(String type, int month, int year, OutputStream os) {
		XSSFWorkbook workbook = new XSSFWorkbook();
		styles = createStyles(workbook);
		XSSFSheet sheet = workbook.createSheet("All Accounts");
		XSSFRow row;
		
		//title row
		XSSFRow titleRow = sheet.createRow(0);
        titleRow.setHeightInPoints(45);
        XSSFCell titleCell = titleRow.createCell(0);
        titleCell.setCellValue("All Email Accounts Report");
        titleCell.setCellStyle(styles.get("title"));
        sheet.addMergedRegion(CellRangeAddress.valueOf("$A$1:$Q$1"));
        
        XSSFCell cell;
		row = sheet.createRow(1);
//		row.createCell(0).setCellValue("Date");
		cell = row.createCell(0);
		cell.setCellValue("Date");
		cell.setCellStyle(styles.get("header"));
		
		List<EmailAccount> accounts = emailAccountRepo.findAll();
		List<EmailResolution> resolutions = buildHeader();
		
		for (int i = 0; i < resolutions.size(); i++) {
			cell = row.createCell(i + 1);
			cell.setCellValue(resolutions.get(i).getName());
			cell.setCellStyle(styles.get("header"));
		}
//		row.createCell(resolutions.size() + 1).setCellValue("Total Received Emails");
		cell = row.createCell(resolutions.size() + 1);
		cell.setCellValue("Total Received Emails");
		cell.setCellStyle(styles.get("header"));
		
//		row.createCell(resolutions.size() + 2).setCellValue("Total Actions");
		cell = row.createCell(resolutions.size() + 2);
		cell.setCellValue("Total Actions");
		cell.setCellStyle(styles.get("header"));
		
//		row.createCell(resolutions.size() + 3).setCellValue("Total No-Action/Junk");
		cell = row.createCell(resolutions.size() + 3);
		cell.setCellValue("Total No-Action/Junk");
		cell.setCellStyle(styles.get("header"));
		
//		int colAfterRes = resolutions.size() + 4;
//		for (EmailAccount account : accounts) {
//			row.createCell(colAfterRes).setCellValue(account.getUsername());
//			colAfterRes++;
//		}
		
		//Totals Sheet
		if (type.equals("monthly")) {
			writeMonthlyRowsForAllAccounts(month, year, row, sheet, resolutions, accounts);
		} else {
			writeAnnualRowsForAllAccounts(year, month, row, sheet, resolutions, accounts);
		}
		
		//Each Account Sheet
		for (EmailAccount account : accounts) {
			sheet = workbook.createSheet(account.getUsername());
			
			titleRow = sheet.createRow(0);
	        titleRow.setHeightInPoints(45);
	        titleCell = titleRow.createCell(0);
	        titleCell.setCellValue(account.getUsername()+" Report");
	        titleCell.setCellStyle(styles.get("title"));
	        sheet.addMergedRegion(CellRangeAddress.valueOf("$A$1:$Q$1"));
	        
			row = sheet.createRow(1);
//			row.createCell(0).setCellValue("Date");
			cell = row.createCell(0);
			cell.setCellValue("Date");
			cell.setCellStyle(styles.get("header"));
			
			for (int i = 0; i < resolutions.size(); i++) {
				cell = row.createCell(i + 1);
				cell.setCellValue(resolutions.get(i).getName());
				cell.setCellStyle(styles.get("header"));
			}
//			row.createCell(resolutions.size() + 1).setCellValue("Total Received Emails");
//			row.createCell(resolutions.size() + 2).setCellValue("Total Actions");
//			row.createCell(resolutions.size() + 3).setCellValue("Total No-Action/Junk");
			cell = row.createCell(resolutions.size() + 1);
			cell.setCellValue("Total Received Emails");
			cell.setCellStyle(styles.get("header"));
			
			cell = row.createCell(resolutions.size() + 2);
			cell.setCellValue("Total Actions");
			cell.setCellStyle(styles.get("header"));
			
			cell = row.createCell(resolutions.size() + 3);
			cell.setCellValue("Total No-Action/Junk");
			cell.setCellStyle(styles.get("header"));
			
			if (type.equals("monthly")) {
				writeMonthlyRowsForAccount(month, year, row, sheet, resolutions, account);
			} else {
				writeAnnualRowsForAccount(year, month, row, sheet, resolutions, account);
			}
		}

		try {
			workbook.write(os);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				workbook.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	private void writeMonthlyRowsForAllAccounts(int month, int year, XSSFRow row, XSSFSheet sheet, List<EmailResolution> resolutions, List<EmailAccount> accounts) {
		XSSFCell cell;
		List<String> dates = datesMonthly(month, year);
		for (int i = 0; i < dates.size(); i++) {
			row = sheet.createRow(i + 2);
			cell = row.createCell(0);
			cell.setCellValue(dates.get(i));
			cell.setCellStyle(styles.get("header"));
			
			int col = 1;
			for (EmailResolution resolution : resolutions) {
				cell = row.createCell(col);
				cell.setCellValue(actionRepo
						.getAllByResolutionAndDate(year, month, getDay(dates.get(i)), resolution.getId()).size());
				cell.setCellStyle(styles.get("cell"));
				col++;
			}

			// Total Received Emails
			cell = row.createCell(col);
			cell.setCellValue(
					emailRepo.getAllEmailsOfDayAndMonthAndYear(getDay(dates.get(i)), month, year).size());
			cell.setCellStyle(styles.get("cell"));
			col++;
			
			// Total Actions
			cell = row.createCell(col);
			cell.setCellValue(
					actionRepo.getAllActionsOfDayAndMonthAndYear(getDay(dates.get(i)), month, year).size());
			cell.setCellStyle(styles.get("cell"));
			col++;
			
			// Total No-Action/Junk
			cell = row.createCell(col);
			cell.setCellValue(
					actionRepo.getAllNoActionOfDayAndMonthAndYear(getDay(dates.get(i)), month, year).size());
			cell.setCellStyle(styles.get("cell"));
			col++;

			// By Account
//			for (EmailAccount account : accounts) {
//				row.createCell(col)
//						.setCellValue(emailRepo.getAllEmailsOfDayAndMonthAndYearAndAccount(getDay(dates.get(i)),
//								month, year, account.getId()).size());
//				col++;
//			}

		}
		row = sheet.createRow(dates.size() + 2);
		cell = row.createCell(0);
		cell.setCellValue("Total");
		cell.setCellStyle(styles.get("header"));
		
		int col = 1;
		for (EmailResolution resolution : resolutions) {
			cell = row.createCell(col);
			cell.setCellValue(
					actionRepo.getAllByResolutionAndYearAndMonth(year, month, resolution.getId()).size());
			cell.setCellStyle(styles.get("totalCell"));
			col++;
		}
		cell = row.createCell(col);
		cell.setCellValue(emailRepo.getAllEmailsOfMonthAndYear(month, year).size());
		cell.setCellStyle(styles.get("totalCell"));
		col++;
		
		cell = row.createCell(col);
		cell.setCellValue(actionRepo.getAllActionsOfMonthAndYear(month, year).size());
		cell.setCellStyle(styles.get("totalCell"));
		col++;
		
		cell = row.createCell(col);
		cell.setCellValue(actionRepo.getAllNoActionOfMonthAndYear(month, year).size());
		cell.setCellStyle(styles.get("totalCell"));
		col++;

//		for (EmailAccount account : accounts) {
//			row.createCell(col).setCellValue(
//					emailRepo.getAllEmailsOfMonthAndYearAndAccount(month, year, account.getId()).size());
//			col++;
//		}

		for (int i = 0; i < resolutions.size() + 7; i++) {
			sheet.autoSizeColumn(i);
		}
	}
	
	private void writeAnnualRowsForAllAccounts(int year, int month, XSSFRow row, XSSFSheet sheet, List<EmailResolution> resolutions, List<EmailAccount> accounts) {
		XSSFCell cell;
		List<String> dates = datesAnnual(year);
		for (int i = 0; i < dates.size(); i++) {
			row = sheet.createRow(i + 2);
			cell = row.createCell(0);
						
			cell.setCellValue(dates.get(i));
			cell.setCellStyle(styles.get("header"));

			int col = 1;
			for (EmailResolution resolution : resolutions) {
				month = getMonth(dates.get(i));
				cell = row.createCell(col);
				cell.setCellValue(
						actionRepo.getAllByResolutionAndYearAndMonth(year, month, resolution.getId()).size());
				cell.setCellStyle(styles.get("cell"));
				col++;
			}
			cell = row.createCell(col);
			cell.setCellValue(emailRepo.getAllEmailsOfMonthAndYear(month, year).size());
			cell.setCellStyle(styles.get("cell"));
			col++;
			
			// Total Actions
			cell = row.createCell(col);
			cell.setCellValue(
					actionRepo.getAllActionsOfMonthAndYear(month, year).size());
			cell.setCellStyle(styles.get("cell"));
			col++;
			
			// Total No-Action/Junk
			cell = row.createCell(col);
			cell.setCellValue(
					actionRepo.getAllNoActionOfMonthAndYear(month, year).size());
			cell.setCellStyle(styles.get("cell"));
			col++;
			
			// By Account
//			for (EmailAccount account : accounts) {
//				row.createCell(col).setCellValue(
//						emailRepo.getAllEmailsOfMonthAndYearAndAccount(month, year, account.getId()).size());
//				col++;
//			}
		}
		row = sheet.createRow(dates.size() + 2);
		cell = row.createCell(0);
		cell.setCellValue("Total");
		cell.setCellStyle(styles.get("header"));
		
		int col = 1;
		for (EmailResolution resolution : resolutions) {
			cell = row.createCell(col);
			cell.setCellValue(actionRepo.getAllByResolutionAndYear(year, resolution.getId()).size());
			cell.setCellStyle(styles.get("totalCell"));
			col++;
		}
		cell = row.createCell(col);
		cell.setCellValue(emailRepo.getAllEmailsOfYear(year).size());
		cell.setCellStyle(styles.get("totalCell"));
		col++;
		
		cell = row.createCell(col);
		cell.setCellValue(actionRepo.getAllActionsOfYear(year).size());
		cell.setCellStyle(styles.get("totalCell"));
		col++;
		
		cell = row.createCell(col);
		cell.setCellValue(actionRepo.getAllNoActionOfYear(year).size());
		cell.setCellStyle(styles.get("totalCell"));
		col++;

//		for (EmailAccount account : accounts) {
//			row.createCell(col).setCellValue(emailRepo.getAllEmailsOfYearAndAccount(year, account.getId()).size());
//			col++;
//		}

		for (int i = 0; i < resolutions.size() + 7; i++) {
			sheet.autoSizeColumn(i);
		}
	}
	
	private void writeMonthlyRowsForAccount(int month, int year, XSSFRow row, XSSFSheet sheet, List<EmailResolution> resolutions, EmailAccount account) {
		XSSFCell cell;
		List<String> dates = datesMonthly(month, year);
		for (int i = 0; i < dates.size(); i++) {
			row = sheet.createRow(i + 2);
			cell = row.createCell(0);
			cell.setCellValue(dates.get(i));
			cell.setCellStyle(styles.get("header"));
			
			int col = 1;
			for (EmailResolution resolution : resolutions) {
				cell = row.createCell(col);
				cell.setCellValue(actionRepo
						.countByResolutionAndDateAndAccount(year, month, getDay(dates.get(i)), resolution.getId(), account.getId()));
				cell.setCellStyle(styles.get("cell"));
				col++;
			}

			// Total Received Emails
			cell = row.createCell(col);
			cell.setCellValue(
					emailRepo.getAllEmailsOfDayAndMonthAndYearAndAccount(getDay(dates.get(i)), month, year, account.getId()).size());
			cell.setCellStyle(styles.get("cell"));
			col++;
			
			// Total Actions
			cell = row.createCell(col);
			cell.setCellValue(
					actionRepo.countByDateAndAccount(year, month, getDay(dates.get(i)), account.getId()));
			cell.setCellStyle(styles.get("cell"));
			col++;
			
			// Total No-Action/Junk
			cell = row.createCell(col);
			cell.setCellValue(
					actionRepo.countByDateAndAccountAndNullResolution(year, month, getDay(dates.get(i)), account.getId()));
			cell.setCellStyle(styles.get("cell"));
			col++;
		}
		
		row = sheet.createRow(dates.size() + 2);
		cell = row.createCell(0);
		cell.setCellValue("Total");
		cell.setCellStyle(styles.get("header"));
		int col = 1;
		for (EmailResolution resolution : resolutions) {
			cell = row.createCell(col);
			cell.setCellValue(
					actionRepo.countByResolutionAndYearAndMonthAndAccount(year, month, resolution.getId(), account.getId()));
			cell.setCellStyle(styles.get("totalCell"));
			col++;
		}
		cell = row.createCell(col);
		cell.setCellValue(emailRepo.getAllEmailsOfMonthAndYearAndAccount(month, year, account.getId()).size());
		cell.setCellStyle(styles.get("totalCell"));
		col++;
		
		cell = row.createCell(col);
		cell.setCellValue(actionRepo.countByYearAndMonthAndAccount(month, year, account.getId()));
		cell.setCellStyle(styles.get("totalCell"));
		col++;
		
		cell = row.createCell(col);
		cell.setCellValue(actionRepo.countByYearAndMonthAndAccountAndNullResolution(year, month, account.getId()));
		cell.setCellStyle(styles.get("totalCell"));
		col++;

		for (int i = 0; i < resolutions.size() + 7; i++) {
			sheet.autoSizeColumn(i);
		}
	}
	
	private void writeAnnualRowsForAccount(int year, int month, XSSFRow row, XSSFSheet sheet, List<EmailResolution> resolutions, EmailAccount account) {
		XSSFCell cell;
		List<String> dates = datesAnnual(year);
		for (int i = 0; i < dates.size(); i++) {
			row = sheet.createRow(i + 2);
			cell = row.createCell(0);
			cell.setCellValue(dates.get(i));
			cell.setCellStyle(styles.get("header"));

			int col = 1;
			for (EmailResolution resolution : resolutions) {
				month = getMonth(dates.get(i));
				cell = row.createCell(col);
				cell.setCellValue(
						actionRepo.countByResolutionAndYearAndMonthAndAccount(year, month, resolution.getId(), account.getId()));
				cell.setCellStyle(styles.get("cell"));
				col++;
			}
			cell = row.createCell(col);
			cell.setCellValue(emailRepo.getAllEmailsOfMonthAndYearAndAccount(month, year, account.getId()).size());
			cell.setCellStyle(styles.get("cell"));
			col++;
			
			// Total Actions
			cell=row.createCell(col);
			cell.setCellValue(
					actionRepo.countByYearAndMonthAndAccount(year, month, account.getId()));
			cell.setCellStyle(styles.get("cell"));
			col++;
			
			// Total No-Action/Junk
			cell = row.createCell(col);
			cell.setCellValue(
					actionRepo.countByYearAndMonthAndAccountAndNullResolution(year, month, account.getId()));
			cell.setCellStyle(styles.get("cell"));
			col++;

		}
		row = sheet.createRow(dates.size() + 2);
		cell = row.createCell(0);
		cell.setCellValue("Total");
		cell.setCellStyle(styles.get("header"));
		int col = 1;
		for (EmailResolution resolution : resolutions) {
			cell = row.createCell(col);
			cell.setCellValue(actionRepo.countByResolutionAndYearAndAccount(year, resolution.getId(), account.getId()));
			cell.setCellStyle(styles.get("totalCell"));
			col++;
		}
		cell = row.createCell(col);
		cell.setCellValue(emailRepo.getAllEmailsOfYearAndAccount(year, account.getId()).size());
		cell.setCellStyle(styles.get("totalCell"));
		col++;
		
		cell = row.createCell(col);
		cell.setCellValue(actionRepo.countByYearAndAccount(year, account.getId()));
		cell.setCellStyle(styles.get("totalCell"));
		col++;
		
		cell = row.createCell(col);
		cell.setCellValue(actionRepo.countByYearAndAccountAndNullResolution(year, account.getId()));
		cell.setCellStyle(styles.get("totalCell"));
		col++;


		for (int i = 0; i < resolutions.size() + 7; i++) {
			sheet.autoSizeColumn(i);
		}
	}
	
	/**
     * Create a library of cell styles
     */
    private Map<String, CellStyle> createStyles(Workbook wb){
        Map<String, CellStyle> styles = new HashMap<>();
        CellStyle style;
        Font titleFont = wb.createFont();
        titleFont.setFontHeightInPoints((short)18);
        titleFont.setBold(true);
        style = wb.createCellStyle();
        style.setAlignment(HorizontalAlignment.CENTER);
        style.setVerticalAlignment(VerticalAlignment.CENTER);
        style.setFont(titleFont);
        styles.put("title", style);

        Font monthFont = wb.createFont();
        monthFont.setFontHeightInPoints((short)11);
        monthFont.setColor(IndexedColors.WHITE.getIndex());
        style = wb.createCellStyle();
        style.setAlignment(HorizontalAlignment.CENTER);
        style.setVerticalAlignment(VerticalAlignment.CENTER);
        style.setFillForegroundColor(IndexedColors.GREY_50_PERCENT.getIndex());
        style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        style.setFont(monthFont);
        style.setWrapText(true);
        styles.put("header", style);

        style = wb.createCellStyle();
        style.setAlignment(HorizontalAlignment.CENTER);
        style.setWrapText(true);
        style.setBorderRight(BorderStyle.THIN);
        style.setRightBorderColor(IndexedColors.BLACK.getIndex());
        style.setBorderLeft(BorderStyle.THIN);
        style.setLeftBorderColor(IndexedColors.BLACK.getIndex());
        style.setBorderTop(BorderStyle.THIN);
        style.setTopBorderColor(IndexedColors.BLACK.getIndex());
        style.setBorderBottom(BorderStyle.THIN);
        style.setBottomBorderColor(IndexedColors.BLACK.getIndex());
        styles.put("cell", style);
        
        style = wb.createCellStyle();
        style.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
        style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        style.setAlignment(HorizontalAlignment.CENTER);
        style.setWrapText(true);
        style.setBorderRight(BorderStyle.THIN);
        style.setRightBorderColor(IndexedColors.BLACK.getIndex());
        style.setBorderLeft(BorderStyle.THIN);
        style.setLeftBorderColor(IndexedColors.BLACK.getIndex());
        style.setBorderTop(BorderStyle.THIN);
        style.setTopBorderColor(IndexedColors.BLACK.getIndex());
        style.setBorderBottom(BorderStyle.THIN);
        style.setBottomBorderColor(IndexedColors.BLACK.getIndex());
        styles.put("totalCell", style);

//        style = wb.createCellStyle();
//        style.setAlignment(HorizontalAlignment.CENTER);
//        style.setVerticalAlignment(VerticalAlignment.CENTER);
//        style.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
//        style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
//        style.setDataFormat(wb.createDataFormat().getFormat("0.00"));
//        styles.put("formula", style);

//        style = wb.createCellStyle();
//        style.setAlignment(HorizontalAlignment.CENTER);
//        style.setVerticalAlignment(VerticalAlignment.CENTER);
//        style.setFillForegroundColor(IndexedColors.GREY_40_PERCENT.getIndex());
//        style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
//        style.setDataFormat(wb.createDataFormat().getFormat("0.00"));
//        styles.put("formula_2", style);

        return styles;
    }

}
