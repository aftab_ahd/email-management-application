package com.jarir.ems.services;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import org.keycloak.adapters.springsecurity.client.KeycloakRestTemplate;
import org.keycloak.representations.idm.UserRepresentation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserRetrievalService{

    @Autowired
    private KeycloakRestTemplate keycloakRestTemplate;

    public UserRepresentation[] getUsersByName(String name) {
    	name = name.replaceAll(" ", "+");
    	UserRepresentation[] users = keycloakRestTemplate.getForObject(
				URI.create(
						"https://ho-sson1.jarirbookstore.com:8543/auth/admin/realms/master/users?" + String.format("max=6&firstName=%s", name)),
				UserRepresentation[].class);
    	List<UserRepresentation> finalUsersList = new ArrayList<>();
    	for(UserRepresentation user: users) {
    		if(user.getEmail()!=null)
    			finalUsersList.add(user);
    	}
    	
    	return finalUsersList.toArray(new UserRepresentation[finalUsersList.size()]);
    }

}