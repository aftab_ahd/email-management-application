package com.jarir.ems.services;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import javax.mail.Address;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.Message.RecipientType;
import javax.mail.MessagingException;
import javax.mail.Part;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.UIDFolder;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.jarir.ems.entities.ems.AutoEmail;
import com.jarir.ems.entities.ems.EmailAccount;
import com.jarir.ems.entities.ems.EmailAction;
import com.jarir.ems.entities.ems.EmailAutoreplyRecord;
import com.jarir.ems.entities.ems.JarirEmail;
import com.jarir.ems.entities.ems.Ticket;
import com.jarir.ems.repos.ems.AutoEmailRepository;
import com.jarir.ems.repos.ems.EmailAccountRepository;
import com.jarir.ems.repos.ems.EmailActionRepository;
import com.jarir.ems.repos.ems.EmailAutoreplyRecordRepository;
import com.jarir.ems.repos.ems.JarirEmailRepository;
import com.jarir.ems.repos.ems.TicketRepository;
import com.jarir.ems.util.EmailReadingUtils;
import com.jarir.ems.util.EmailTemplateStrings;

@Service
public class JarirEmailService {

	@Autowired
	JarirEmailRepository emailRepo;
	
	@Autowired
	EmailAccountRepository emailAccountRepo;
	
	@Autowired
	EmailActionRepository emailActionRepository;
	
	@Autowired
	EmailAutoreplyRecordRepository emailAutoreplyRecordRepo;
	
	@Autowired
	AutoEmailRepository autoEmailRepo;
	
	@Autowired
	TicketRepository ticketRepo;
	
	EmailTemplateStrings templateStrings = new EmailTemplateStrings();
	EmailReadingUtils emailUtils = new EmailReadingUtils();
	
	@SuppressWarnings("unused")
	private boolean isAutoReplySent(JarirEmail email) {
		return emailAutoreplyRecordRepo.findByJarirEmail(email) != null;
	}
	
	private boolean isExistingMessageId(JarirEmail email) {
		return emailRepo.findOneByMessageId(email.getMessageId())!=null;
	}
	
	public void sendReply(EmailAction action, Folder folder) throws MessagingException, IOException {
		
		
		UIDFolder uf = (UIDFolder) folder;
		Message message = uf.getMessageByUID(action.getJarirEmail().getEmailUid());
		
		String emailTemplateStart = templateStrings.getEmailTemplateStart();
		String emailTemplateEnd = templateStrings.getEmailTemplateEnd();
	    String replyText = emailTemplateStart;
	    
	    if(action.getEmailTemplate()!=null) {
	    	String templateString = "";
		    if(action.getLanguage().equals("Arabic")) {
		    	templateString = emailUtils.includeParameters(action.getEmailTemplate().getBodyArabic(), action.getParameters());
		    	templateString = templateString.replaceAll("(\r\n|\r|\n|\n\r)", "<br />");
		    	replyText = replyText.replace("${arabic_style_template}", "text-align: right; direction: rtl;");
		    	replyText = replyText.replace("${align}", "align=\"right\"");
		    	replyText += templateString;
		    }else {
		    	templateString = emailUtils.includeParameters(action.getEmailTemplate().getBodyEnglish(), action.getParameters());
		    	templateString = templateString.replaceAll("(\r\n|\r|\n|\n\r)", "<br />");
		    	replyText = replyText.replace("${arabic_style_template}", "");
		    	replyText = replyText.replace("${align}", "");
		    	replyText += templateString;
		    }
	    
		    replyText += emailTemplateEnd;
		    
		   // String text = action.getJarirEmail().getBody();
		    //boolean isBodyArabic = emailUtils.isProbablyArabic(text);
		    
//		    if(action.getLanguage().equals("Arabic")) {
//		    	emailTemplateEnd = emailTemplateEnd.replace("${arabic_style_template}", "text-align: right; direction: rtl;");
//		    	emailTemplateEnd = emailTemplateEnd.replace("${align}", "align=\"right\"");
//		    } else if(action.getLanguage().equals("English")) {
//		    	emailTemplateEnd = emailTemplateEnd.replace("${arabic_style_teplate}", "");
//		    	emailTemplateEnd = emailTemplateEnd.replace("${align}", "");
//		    }
		    
//		    if(isBodyArabic) {
//		    	emailTemplateEnd = emailTemplateEnd.replace("${arabic_style_body}", "text-align: right; direction: rtl;");
//		    	text = text.replaceAll("(?m)^", " <");
//		    } else {
//		    	emailTemplateEnd = emailTemplateEnd.replace("${arabic_style_body}", "");
//		    	text = text.replaceAll("(?m)^", "> ");
//		    }
//		    
//		    text = text.replaceAll("(\r\n|\r|\n|\n\r){3,}", "<br /><br />");
//		    text = text.replaceAll("(\r\n|\r|\n|\n\r){1,}", "<br />");
//		    text = text.replaceAll("(\r\n|\r|\n|\n\r)", "<br />");
		    
		    //replyText += emailTemplateEnd.replace("${original_email_body}", text);
		    
		    
		    
		    MimeMessage reply = (MimeMessage) message.reply(false);
		    reply.setText(replyText, "utf-8", "html");
		    reply.setFrom(new InternetAddress(InternetAddress.toString(message.getRecipients(Message.RecipientType.TO))));
		    reply.setReplyTo(message.getReplyTo());
		    
		    Properties properties = new Properties();
			properties.put("mail.smtp.auth", "true");
			properties.put("mail.smtp.starttls.enable", "true");
			properties.put("mail.smtp.host", "jbemail01.jarirbookstore.com");
			properties.put("mail.smtp.port", "25");
			Session session = Session.getInstance(properties);
		    
		    Transport t = session.getTransport("smtp");
			try {
				t.connect(action.getJarirEmail().getEmailAccount().getUsername(), action.getJarirEmail().getEmailAccount().getUserpassword());
				t.sendMessage(reply, reply.getAllRecipients());
			} finally {
				t.close();
			}
			System.out.println("Replied Successfully");
	    }
	}
	
	public void sendAutoReply(JarirEmail email, Folder folder) throws MessagingException, IOException {
		UIDFolder uf = (UIDFolder) folder;
		Message message = uf.getMessageByUID(email.getEmailUid());
		
		if(message!=null) {
			MimeMessage reply = (MimeMessage) message.reply(false);
		    reply.setFrom(new InternetAddress(email.getEmailAccount().getUsername()));
		    
	//	    reply.setText(templateStrings.getEmailAutoReply(), "utf-8", "html");
		    
		    String body = templateStrings.getEmailAutoReplyDynamic();
		    AutoEmail autoReply = autoEmailRepo.findOneByName("auto-reply");
		    body = body.replace("${auto-reply-eng-body}", autoReply.getEngBody());
		    body = body.replace("${auto-reply-ara-body}", autoReply.getAraBody());
		    reply.setText(body, "utf-8", "html");
		    
		    Properties properties = new Properties();
			properties.put("mail.smtp.auth", "true");
			properties.put("mail.smtp.starttls.enable", "true");
			properties.put("mail.smtp.host", "jbemail01.jarirbookstore.com");
			properties.put("mail.smtp.port", "25");
			Session session = Session.getInstance(properties);
		    
		    Transport t = session.getTransport("smtp");
			try {
	//			t.connect("care-test@jarir.com", "Jarir123");
				t.connect(email.getEmailAccount().getUsername(), email.getEmailAccount().getUserpassword());
				t.sendMessage(reply, message.getReplyTo());
			} finally {
				t.close();
			}
			
			EmailAutoreplyRecord autoReplyRecord = new EmailAutoreplyRecord();
		    autoReplyRecord.setJarirEmail(email);
		    emailAutoreplyRecordRepo.save(autoReplyRecord);
			
			System.out.println("Auto-Replied Successfully ");
		} else {
			System.out.println("Error Sending Auto Reply.  Cannot find Message with uid " + email.getEmailUid());
		}
	}

	public void forward(EmailAction action, Folder folder) throws MessagingException {
		JarirEmail email = action.getJarirEmail();
		EmailAccount account = email.getEmailAccount();
		UIDFolder uf = (UIDFolder) folder;
		Message message = uf.getMessageByUID(email.getEmailUid());
	    
	    Properties properties = new Properties();
		properties.put("mail.smtp.auth", "true");
		properties.put("mail.smtp.starttls.enable", "true");
		properties.put("mail.smtp.host", "jbemail01.jarirbookstore.com");
		properties.put("mail.smtp.port", "25");
		Session session = Session.getInstance(properties);
	    
	    MimeMessage forward = new MimeMessage(session);
	    // set To, From, Subject, Date, etc.
	    MimeMultipart mp = new MimeMultipart();
	    // create a body part to contain whatever text you want to include
	    MimeBodyPart mbp = new MimeBodyPart();
//	    mbp.setText("Dear, \n FYI\n\nCustomer Care");
	    AutoEmail forwardTo = autoEmailRepo.findOneByName("forward-to");
	    
	    mbp.setText(forwardTo.getEngBody().replaceAll("(\r\n|\r|\n|\n\r)", "<br />"), "utf-8", "html");
	    
	    mp.addBodyPart(mbp);
	    // create another body part to contain the message to be forwarded
	    MimeBodyPart fmbp = new MimeBodyPart();
	    // forwardedMsg is the MimeMessage object you want to forward as an attachment
	    fmbp.setContent(message, "message/rfc822");
	    fmbp.setDisposition(Part.ATTACHMENT);
	    mp.addBodyPart(fmbp);
	    forward.setContent(mp);
	    forward.setFrom(new InternetAddress(account.getUsername()));
	    forward.setSubject("Fwd: " + message.getSubject());
	    
	    //METHOD 2
//	    String text = (String)forwardedMsg.getContent();
//	    String forwardedText = String.format(
//		"\n\n-------- Original Message --------\n" +
//		"Subject: %s\nDate: %s\nFrom: %s\nTo: %s\n",
//		forward.getSubject(),
//		forward.getSentDate(),
//		forward.getFrom()[0],
//		formatAddressList(
//		  forwardedMsg.getRecipients(Message.RecipientType.TO)));
//	    // allow user to edit forwardedText,
//	    // e.g., using a Swing GUI or a web form
//	    msg.setText(forwardedText);
	    
	    Transport t = session.getTransport("smtp");
		try {
//			t.connect("care-test@jarir.com", "Jarir123");
			t.connect(email.getEmailAccount().getUsername(), email.getEmailAccount().getUserpassword());
			forward.setRecipients(RecipientType.TO, new Address[] {new InternetAddress(action.getForwardTo())});
			t.sendMessage(forward, new Address[] {new InternetAddress(action.getForwardTo())});

		} finally {
			t.close();
		}
		
		System.out.println("Forwarded Successfully ");
		
	}
	
	public void saveNewEmails(List<JarirEmail> allEmails, Folder folder) {
		for(JarirEmail email : allEmails) {
			if(!isExistingMessageId(email)) {
				saveNewEmail(email);
				try {
					//Send auto-reply email only if email is received within the past 24hrs
					Date today = new Date();
					long hoursdiff = TimeUnit.MILLISECONDS.toHours(today.getTime()-email.getReceivedDate().getTime());
					if(hoursdiff < 24) {
						//Do not send another auto-reply if they sent email before 24hrs
						//if(emailRepo.findByfromEmailForLastDay(email.getFromEmail()).isEmpty())
						//	sendAutoReply(email, folder);
						Date now = new Date();
						Calendar cal = Calendar.getInstance();
						cal.setTime(now);
						cal.add(Calendar.DATE, -1);
						Date dateMinus1Day = cal.getTime();
						
						List<JarirEmail> emails = emailRepo.findAllByFromEmailAndReceivedDateBetween(email.getFromEmail(), dateMinus1Day, now);
						
//						if(emails.size() == 1) {
//							sendAutoReply(email, folder);	
//						}
					}
				} 
				catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	final private String PATTERN = "<([A-Z]|[0-9]){10}\\.([A-Z]|[0-9]){8}-([A-Z]|[0-9]){10}\\.([A-Z]|[0-9]){8}-([A-Z]|[0-9]){8}\\.([A-Z]|[0-9]){8}@.*";
	
	public boolean isMessageIdEquals(String messageId1, String messageId2) {
		if(messageId1.matches(PATTERN) && messageId2.matches(PATTERN)) {
			if(messageId1.substring(1, 20).equals(messageId2.substring(1, 20))) {
				return true;
			}
		} else if(messageId1.equals(messageId2)) {
			return true;
		}
		return false;
	}
	
	private List<JarirEmail> findReferencedEmails(JarirEmail email){
		List<JarirEmail> referencedEmails = new ArrayList<>();
		if(!email.getReferencesEmails().isEmpty()) {
			String[] references = email.getReferencesEmails().split(" ");
			for(int i = 0 ; i < references.length; i++) {
				references[i] = references[i].replace("\n", "").replace("\r", "");
				for(JarirEmail emailToCheck : emailRepo.findAll()) {
					String messageIdToCheck = emailToCheck.getMessageId();
					if(isMessageIdEquals(references[i], messageIdToCheck)) {
						referencedEmails.add(emailToCheck);
					}
				}
			}
		}
		return referencedEmails;
	}
	
	private void saveNewEmail(JarirEmail email) {
//		email.setStatus("Pending");
//		emailRepo.save(email);
//		
//		if(email.getTicket()==null) {
//			email.setStatus("Pending");
//			emailRepo.save(email);
//		} else {
//			if (ticket.getStatus().equals("Closed")) {
//				ticket.setStatus("Pending");
//			}
//			ticket.setHasUpdates(true);
//			email.setStatus("Pending");
//			email.setTicket(ticket);
//			ticket.getEmails().add(email);
//			ticketRepo.save(ticket);
//			// emailRepo.save(email);
//		}
		
		// If References an Email - Add to ticket
		List<JarirEmail> referencedEmails = findReferencedEmails(email);
		if(referencedEmails.isEmpty()) {
			Ticket ticket = new Ticket();
			ticket.setStatus("Pending");
			ticket.setHasUpdates(true);
			email.setTicket(ticket);
			email.setStatus("Pending");
			ticket.getEmails().add(email);
			ticketRepo.save(ticket);
			emailRepo.save(email);
		} else {
			JarirEmail lastReferenced = referencedEmails.get(0);
			Ticket ticket = ticketRepo.findById(lastReferenced.getTicket().getId()).get();
			if(ticket!=null) {
				if(ticket.getStatus().equals("Closed")) {
					ticket.setStatus("Pending");
				}
				ticket.setHasUpdates(true);
				email.setStatus("Pending");
				email.setTicket(ticket);
				ticket.getEmails().add(email);
				ticketRepo.save(ticket);
				//emailRepo.save(email);
			} else {
				System.out.println("======> No ticket for referenced??");
				email.setStatus("Pending");
				emailRepo.save(email);
			}
		}
		
	}
	
	public List<JarirEmail> getMessagesUsingImap() {
		List<EmailAccount> accounts = emailAccountRepo.findAll();
		List<JarirEmail> allEmails = new ArrayList<>();
		Folder folder = null;
		for(EmailAccount account : accounts) {
			try {
				folder = emailUtils.readEmailFolder(account.getHost(), account.getUsername(), account.getUserpassword());
				List<JarirEmail> parsedEmails = emailUtils.parseMessages(folder, account);
				allEmails.addAll(parsedEmails);
			} catch (MessagingException e) {
				e.printStackTrace();
			}			
		}
		allEmails.sort((o1, o2) -> o1.getSentDate().compareTo(o2.getSentDate()));
		return allEmails;
	}

	public void syncEmails() {
		System.out.println("syncEmails -- > start");
		List<EmailAccount> accounts = emailAccountRepo.findAll();
		List<JarirEmail> allEmails = new ArrayList<>();
		Folder folder = null;
		for(EmailAccount account : accounts) {
			try {
				folder = emailUtils.readEmailFolder(account.getHost(), account.getUsername(), account.getUserpassword());
				List<JarirEmail> parsedEmails = emailUtils.parseMessages(folder, account);
				allEmails.addAll(parsedEmails);
			} catch (MessagingException e) {
				e.printStackTrace();
			}			
		}
		allEmails.sort((o1, o2) -> o1.getSentDate().compareTo(o2.getSentDate()));
		
		saveNewEmails(allEmails, folder);	
		System.out.println("syncEmails -- > end");
	}

	@Async
	@Transactional(propagation = Propagation.REQUIRED)
	public void handleAction(EmailAction action) {
		EmailAccount account = action.getJarirEmail().getEmailAccount();
		try {
			Folder folder = emailUtils.readEmailFolder(account.getHost(), account.getUsername(), account.getUserpassword());
			sendReply(action, folder);
			if(!action.getForwardTo().isEmpty())
				forward(action, folder);
		} catch (MessagingException | IOException  e1) {
			e1.printStackTrace();
		}
		
//		emailRepo.save(action.getJarirEmail());
//		moveEmailToFolders(action);
		try {
			emailUtils.moveEmail(action.getJarirEmail(), "EMS_ACTIONS");
		} catch (MessagingException e) {
			e.printStackTrace();
		}
	}
	
	@Async
	@Transactional(propagation = Propagation.REQUIRED)
	public void handleNoAction(JarirEmail email) {
		try {
			emailUtils.moveEmail(email, "EMS_NO_ACTIONS");
		} catch (MessagingException e) {
			e.printStackTrace();
		}
//		moveEmailToFolders(action);
	}

//	public void saveAction(EmailAction action) {
//		emailActionRepository.save(action);
//		moveEmailToFolders(action);
//	}
	
//	private void moveEmailToFolders(EmailAction action) {
////		System.out.println("Action Null? " + (action==null));
//		System.out.println("moveEmailToFolders Email Null? " + (action.getJarirEmail()==null));
////		System.out.println("Status Null? " + (action.getJarirEmail().getStatus()==null));
//		try {
//			if(action.getJarirEmail().getStatus().equals("NoAction")) {
//				emailUtils.moveEmail(action.getJarirEmail(), "EMS_NO_ACTIONS");
//			} else {
//				emailUtils.moveEmail(action.getJarirEmail(), "EMS_ACTIONS");
//			}
//		} catch (MessagingException e) {
//			e.printStackTrace();
//		}
//	}

}
