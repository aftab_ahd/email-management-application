package com.jarir.ems.sechduled;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.jarir.ems.services.JarirEmailService;

@Component
@EnableScheduling
public class SyncEmails {
	private static final Logger logger = LoggerFactory.getLogger(SyncEmails.class);
	
	@Autowired
	JarirEmailService emailService;
	
	@Scheduled(fixedDelay = 50000)
	private void syncEmailsEvery10Seconds() {
		logger.info(
	      "Fixed delay SyncEmails - " + System.currentTimeMillis() / 1000);
		System.out.println(
			      "Fixed delay SyncEmails - " + System.currentTimeMillis() / 1000);
		emailService.syncEmails();
	}

}
