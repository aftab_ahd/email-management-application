package com.jarir.ems;

import java.util.List;

import javax.mail.MessagingException;

import org.springframework.beans.factory.annotation.Autowired;

import com.jarir.ems.entities.ems.EmailAccount;
import com.jarir.ems.repos.ems.EmailAccountRepository;
import com.jarir.ems.util.EmailReadingUtils;

public class InitEmailFolders {
		
	@Autowired
	EmailAccountRepository accountRepo;
	EmailReadingUtils emailUtils = new EmailReadingUtils();
	
	public void init() {
		List<EmailAccount> accounts = accountRepo.findAll();
		for(EmailAccount account : accounts) {
			try {
				emailUtils.creatEmailFolders(account);
			} catch (MessagingException e) {
				e.printStackTrace();
			}
		}
	}
}
