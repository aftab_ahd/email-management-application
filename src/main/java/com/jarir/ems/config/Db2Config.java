package com.jarir.ems.config;

//import java.util.Properties;
//
//import javax.naming.NamingException;
//import javax.sql.DataSource;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Qualifier;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.core.env.Environment;
//import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
//import org.springframework.jdbc.core.JdbcTemplate;
//import org.springframework.jdbc.datasource.DriverManagerDataSource;
//import org.springframework.jndi.JndiObjectFactoryBean;
//import org.springframework.orm.jpa.JpaTransactionManager;
//import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
//import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
//import org.springframework.transaction.PlatformTransactionManager;

//@Configuration
//@EnableJpaRepositories(basePackages = "com.jarir.ems.repos.db2", entityManagerFactoryRef = "db2EntityManagerFactory", transactionManagerRef = "db2TansactionManager")
public class Db2Config {

//	@Autowired
//	private Environment env;
//	
//	@Bean(name = "db2JdbcTemplate")
//	@Qualifier("db2Jdbc")
//	public JdbcTemplate db2JdbcTemplate(@Qualifier("dsDB2") DataSource dsDB2) {
//		return new JdbcTemplate(dsDB2);
//	}
//
//	@Bean(name="db2EntityManagerFactory")
//	public LocalContainerEntityManagerFactoryBean db2EntityManagerFactory() throws IllegalArgumentException, NamingException {
//		LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
//		em.setDataSource(dataSource());
//		em.setPackagesToScan("com.jarir.ems.entities.db2");
//
//		HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
//		em.setJpaVendorAdapter(vendorAdapter);
//		
//		em.setJpaProperties(additionalProperties());
//
//		return em;
//	}
//
//	@Bean(name="dsDB2")
//	public DataSource dataSource() throws IllegalArgumentException, NamingException {
//		DriverManagerDataSource dataSource = new DriverManagerDataSource();
//		dataSource.setDriverClassName(env.getProperty("ems.database.db2.driver"));
//		dataSource.setUrl(env.getProperty("ems.database.db2.url"));
//		dataSource.setUsername(env.getProperty("ems.database.db2.user"));
//		dataSource.setPassword(env.getProperty("ems.database.db2.pass"));
//		return dataSource;
//		
////		JndiObjectFactoryBean bean = new JndiObjectFactoryBean();
////	    bean.setJndiName("jdbc/db2");
////	    bean.setProxyInterface(DataSource.class);
////	    bean.setLookupOnStartup(false);
////	    bean.afterPropertiesSet();
////	    return (DataSource)bean.getObject();
//	}
//
//	Properties additionalProperties() {
//		Properties properties = new Properties();
//		properties.setProperty("hibernate.temp.use_jdbc_metadata_defaults", "false");
//		properties.setProperty("hibernate.hbm2ddl.auto", "validate");
//		properties.setProperty("hibernate.dialect", env.getRequiredProperty("ems.database.db2.dialect"));
//		properties.setProperty("hibernate.show_sql", env.getRequiredProperty("ems.database.db2.show-sql"));
//		properties.setProperty("jadira.usertype.autoRegisterUserTypes", "true");
//		return properties;
//	}
//	
//	@Bean(name="db2TansactionManager")
//	public PlatformTransactionManager db2TansactionManager() throws IllegalArgumentException, NamingException {
//		JpaTransactionManager transactionManager = new JpaTransactionManager();
//		transactionManager.setEntityManagerFactory(db2EntityManagerFactory().getObject());
//		return transactionManager;
//	}
}
