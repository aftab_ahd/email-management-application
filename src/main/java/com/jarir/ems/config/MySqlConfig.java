package com.jarir.ems.config;

import java.util.HashMap;
import java.util.Properties;

import javax.naming.NamingException;
import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.env.Environment;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jndi.JndiObjectFactoryBean;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;

@Configuration
@EnableJpaRepositories(basePackages = "com.jarir.ems.repos.ems", entityManagerFactoryRef = "mysqlEntityManager", transactionManagerRef = "mysqlTransactionManager")
public class MySqlConfig {

	Logger logger = LoggerFactory.getLogger(MySqlConfig.class);
	
	@Autowired
	private Environment env;

	@Bean(name = "emsJdbcTemplate")
	@Qualifier("emsJdbc")
	public JdbcTemplate emsJdbcTemplate(@Qualifier("mysqlDataSource") DataSource mysqlDataSource) {
		return new JdbcTemplate(mysqlDataSource);
	}
	
	@Primary
	@Bean
	public LocalContainerEntityManagerFactoryBean mysqlEntityManager() throws IllegalArgumentException, NamingException {
		LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
		em.setDataSource(mysqlDataSource());
		em.setPackagesToScan("com.jarir.ems.entities.ems");

		HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
		em.setJpaVendorAdapter(vendorAdapter);
		HashMap<String, Object> properties = new HashMap<>();
		properties.put("hibernate.hbm2ddl.auto", env.getRequiredProperty("ems.database.mysql.ddl-auto"));
		properties.put("hibernate.show_sql", env.getRequiredProperty("ems.database.mysql.show-sql"));
		properties.put("hibernate.dialect", env.getRequiredProperty("ems.database.mysql.dialect"));
		em.setJpaPropertyMap(properties);

		return em;
	}

	@Primary
	@Bean
	@Qualifier("mysqlDataSource")
	public DataSource mysqlDataSource() throws IllegalArgumentException, NamingException {
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setDriverClassName(env.getProperty("ems.database.mysql.driver"));
		dataSource.setUrl(env.getProperty("ems.database.mysql.url"));
		dataSource.setUsername(env.getProperty("ems.database.mysql.user"));
		dataSource.setPassword(env.getProperty("ems.database.mysql.pass"));
		return dataSource;
//		JndiObjectFactoryBean bean = new JndiObjectFactoryBean();
//	    bean.setJndiName("jdbc/emailmgtsys");
//	    bean.setProxyInterface(DataSource.class);
//	    bean.setLookupOnStartup(false);
//	    bean.afterPropertiesSet();
//	    return (DataSource)bean.getObject();
	}

	
	@Primary
    @Bean(name="mysqlTransactionManager")
	public PlatformTransactionManager mysqlTransactionManager() throws IllegalArgumentException, NamingException {
		JpaTransactionManager transactionManager = new JpaTransactionManager();
		transactionManager.setEntityManagerFactory(mysqlEntityManager().getObject());
		return transactionManager;
	}

//    @Primary
//    @Bean(name="mysqlTansactionManager")
//    public JpaTransactionManager mysqlTansactionManager() {
//        final JpaTransactionManager transactionManager = new JpaTransactionManager();
//        transactionManager.setEntityManagerFactory(mysqlEntityManager().getObject());
//        return transactionManager;
//    }

    @Primary
    @Bean
    public PersistenceExceptionTranslationPostProcessor exceptionTranslation() {
        return new PersistenceExceptionTranslationPostProcessor();
    }

    protected Properties additionalProperties() {
        final Properties hibernateProperties = new Properties();
        hibernateProperties.setProperty("hibernate.hbm2ddl.auto", env.getProperty("ems.database.mysql.ddl-auto"));
        return hibernateProperties;
    }
}
