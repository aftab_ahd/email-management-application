package com.jarir.ems.config;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Properties;

import javax.mail.Address;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.NoSuchProviderException;
import javax.mail.Part;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.UIDFolder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import com.jarir.ems.entities.ems.AutoEmail;
import com.jarir.ems.entities.ems.EmailAccount;
import com.jarir.ems.entities.ems.EmailResolution;
import com.jarir.ems.entities.ems.EmailTemplateCategory;
import com.jarir.ems.entities.ems.JarirEmail;
import com.jarir.ems.repos.ems.AutoEmailRepository;
import com.jarir.ems.repos.ems.EmailAccountRepository;
import com.jarir.ems.repos.ems.EmailResolutionRepository;
import com.jarir.ems.repos.ems.EmailTemplateCategoryRepository;
import com.jarir.ems.services.JarirEmailService;
import com.sun.mail.util.BASE64DecoderStream;

@Component
public class AppStartupRunner implements ApplicationRunner {
	private static final Logger logger = LoggerFactory.getLogger(AppStartupRunner.class);

	@Autowired
	JarirEmailService emailService;
	
	@Autowired
	EmailAccountRepository emailAccountRepo;
	
	@Autowired
	EmailResolutionRepository emailResolutionRepo;
	
	@Autowired
	EmailTemplateCategoryRepository emailTemplateCategoryRepo;
	
	@Autowired
	AutoEmailRepository autoEmailRepository;
	
	private String[] resolutions = {"Offers", "CRM", "Duplicate", "Inquiry", "Books", "Installment", "HR", "E-Business" , "Corporate Sales" , "After Sales", "Internal", "Sponsoring/Charity" ,"Other" };
	private void createDefaultResolutions() {
		if(emailResolutionRepo.findAll().isEmpty()) {
			for(String reso : resolutions) {
				EmailResolution resolution = new EmailResolution();
				resolution.setName(reso);
				emailResolutionRepo.save(resolution);
			}
		}
	}
	
	private void createTestEmailAccounts() {
		EmailAccount account1 = new EmailAccount();
		account1.setName("Test Account# 1");
		account1.setHost("jbemail01.jarirbookstore.com");
		account1.setUsername("care-test@jarirbookstore.com");
		account1.setUserpassword("Jarir123");
		
		EmailAccount account2 = new EmailAccount();
		account2.setName("Test Account# 2");
		account2.setHost("jbemail01.jarirbookstore.com");
		account2.setUsername("care-test@jarir.com");
		account2.setUserpassword("Jarir123");
		
		if(emailAccountRepo.findOneByUsername(account1.getUsername()) == null)
			emailAccountRepo.save(account1);
		
		if(emailAccountRepo.findOneByUsername(account2.getUsername()) == null)
			emailAccountRepo.save(account2);
	}
	
	private void createAutoEmails() {
		if(autoEmailRepository.findAll().isEmpty()) {
			AutoEmail autoReply = new AutoEmail();
			autoReply.setName("auto-reply");
			autoReply.setAraBody("شكراً لتواصلكم مع مكتبة جرير.\r\n" + 
					"تم إستقبال بريدك الإلكتروني وسيتم الرد عليك خلال يوم عمل واحد.\r\n");
			autoReply.setEngBody("Thank you for contacting Jarir Bookstore.\r\n" + 
					"Your email has been received and will be replied to within one working day.\r\n");
			
			AutoEmail forwardTo = new AutoEmail();
			forwardTo.setName("forward-to");
			forwardTo.setAraBody("شكراً لتواصلكم مع مكتبة جرير.\r\n" );
			forwardTo.setEngBody("Dear, \n FYI\n\nCustomer Care\r\n");
			
			autoEmailRepository.save(autoReply);
			autoEmailRepository.save(forwardTo);
		}
	}
	
	@Override
	public void run(ApplicationArguments args) throws Exception {
		logger.info("Your application started with option names : {}", args.getOptionNames());
//		createTestEmailAccounts();
		createDefaultResolutions();
		createDefaultEmailTemplateCategory();
		createAutoEmails();
	}

	private void createDefaultEmailTemplateCategory() {
		if(emailTemplateCategoryRepo.findAll().isEmpty()) {
			EmailTemplateCategory category = new EmailTemplateCategory();
			category.setName("Default");
			emailTemplateCategoryRepo.save(category);
		}
	}

	int messageCounter = 0;
	public void check(String host, String storeType, String user, String password) {
		try {

			// create properties field
			Properties properties = new Properties();

//			properties.put("mail.pop3.host", host);
//			properties.put("mail.pop3.port", "995");
//			properties.put("mail.pop3.starttls.enable", "true");

//			properties.put("mail.imap.user",user);
//			properties.put("mail.imap.host",host);
//			properties.put("mail.imap.port",143);

			Session emailSession = Session.getDefaultInstance(properties);

			// create the POP3 store object and connect with the pop server
//			Store store = emailSession.getStore("pop3s");
			Store store = emailSession.getStore("imaps");

			store.connect(host, user, password);

			// create the folder object and open it
			Folder emailFolder = store.getFolder("INBOX");
			UIDFolder uf = (UIDFolder)emailFolder;
			emailFolder.open(Folder.READ_ONLY);

			// retrieve the messages from the folder in an array and print it
			Message[] messages = emailFolder.getMessages();
			System.out.println("messages.length---" + messages.length);

			for (int i = 0, n = messages.length; i < n; i++) {
				++messageCounter;
				Message message = messages[i];
				Long messageId = uf.getUID(message);
				System.out.println("---------------------------------");
				System.out.println("Email Number " + (i + 1));
				System.out.println("Email ID " + messageId);
				System.out.println("Subject: " + message.getSubject());
				System.out.println("From: " + message.getFrom()[0]);
				System.out.println("Text: " + message.getContent().toString());
				
				JarirEmail email = new JarirEmail();
				email.setFromEmail(message.getFrom()[0].toString());
				email.setSubject(message.getSubject());
				email.setEmailUid(messageId);
				email.setToEmail(recipientsString(message));
				email.setSentDate(message.getSentDate());
				email.setReceivedDate(message.getReceivedDate());
				//emailService.saveNewEmail(email);
								
				if(message.getHeader("Message-ID") != null) {
					for(int j = 0 ; j < message.getHeader("Message-ID").length; j++)
						System.out.println("Message ID: " + message.getHeader("Message-ID")[j]);
				}
				
				if(message.getHeader("References") != null) {
					for(int j = 0 ; j < message.getHeader("References").length; j++)
						System.out.println("References: " + message.getHeader("References")[j]);
				}
				
				if(message.getHeader("In-Reply-To") != null) {
					for(int j = 0 ; j < message.getHeader("In-Reply-To").length; j++)
						System.out.println("In Reply To: " + message.getHeader("In-Reply-To")[j]);
				}
				
				writePart(message);
//				Multipart multipart = (Multipart) message.getContent();
//				for(int i1=0;i1<multipart.getCount();i1++) {
//				    BodyPart bodyPart = multipart.getBodyPart(i1);
//				    System.out.println(bodyPart.getContentType());
//				    if (bodyPart.isMimeType("text/*")) {
//				        String s = (String) bodyPart.getContent();
//				        System.out.println("Body:"+s);
//				    }
//				}
			}

			// close the store and folder objects
			emailFolder.close(false);
			store.close();

		} catch (NoSuchProviderException e) {
			e.printStackTrace();
		} catch (MessagingException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private String recipientsString(Message m) throws MessagingException {
		String output="";
		String prefix="";
		for(Address address: m.getRecipients(Message.RecipientType.TO)) {
			output = prefix + address.toString();
			prefix=",";
		}
		return output;
	}
	
	/*
	 * This method would print FROM,TO and SUBJECT of the message
	 */
	public void writeEnvelope(Message m) throws Exception {
		System.out.println("This is the message envelope");
		System.out.println("---------------------------");
		Address[] a;

		// FROM
		if ((a = m.getFrom()) != null) {
			for (int j = 0; j < a.length; j++)
				System.out.println("FROM: " + a[j].toString());
		}

		// TO
		if ((a = m.getRecipients(Message.RecipientType.TO)) != null) {
			for (int j = 0; j < a.length; j++)
				System.out.println("TO: " + a[j].toString());
		}

		// SUBJECT
		if (m.getSubject() != null)
			System.out.println("SUBJECT: " + m.getSubject());
	}

	/*
	 * This method checks for content-type based on which, it processes and fetches
	 * the content of the message
	 */
	int counter = 0;
	public void writePart(Part p) throws Exception {
		//if (p instanceof Message)
			// Call methos writeEnvelope
//			writeEnvelope((Message) p);

//		System.out.println("----------------------------");
		System.out.println("CONTENT-TYPE: " + p.getContentType());

		// check if the content is plain text
		if (p.isMimeType("text/plain")) {
			System.out.println("This is plain text");
//			System.out.println("---------------------------");
			System.out.println((String) p.getContent());
		}
		// check if the content has attachment
		else if (p.isMimeType("multipart/*")) {
			System.out.println("This is a Multipart with Length: " + ((Multipart) p.getContent()).getCount());
//			System.out.println("---------------------------");
			Multipart mp = (Multipart) p.getContent();
			int count = mp.getCount();
			for (int i = 0; i < count; i++) {
				System.out.println("calling recursive i = " + i +"/"+count);
				writePart(mp.getBodyPart(i));
			}
		}
		// check if the content is a nested message
		else if (p.isMimeType("message/rfc822")) {
			System.out.println("This is a Nested Message");
//			System.out.println("---------------------------");
			writePart((Part) p.getContent());
		} else if (p.getContentType().contains("image/")) {
//			String contentId = p.getHeader("Content-ID")[0].replace("<", "").replace(">", "");
			System.out.println("Image count:" + (++counter) );
			File f = new File("/tmp/" + messageCounter +"-"+ counter + "."+p.getContentType().split("/")[1]);
			BASE64DecoderStream in = (BASE64DecoderStream) p.getContent();
			FileOutputStream out = new FileOutputStream(f);
			int buf;
			while((buf = in.read()) != -1) {
			     out.write(buf);
			}
//			
			out.close();
			in.close();
			System.out.println(f.getAbsolutePath());

		} else {
			Object o = p.getContent();
			if (o instanceof String) {
				System.out.println("This is a string");
//				System.out.println("---------------------------");
				System.out.println((String) o);
			} else if (o instanceof InputStream) {
				System.out.println("This is just an input stream");
//				System.out.println("---------------------------");
				InputStream is = (InputStream) o;
//				is = (InputStream) o;
				int c;
				while ((c = is.read()) != -1)
					System.out.write(c);
				is.close();
			} else {
				System.out.println("This is an unknown type");
//				System.out.println("---------------------------");
				System.out.println(o.toString());
			}
		}

	}

	public void runMe() {

//		String host = "jarirbookstore.com";
//		String mailStoreType = "pop3";
//		String username = "care-test@jarirbookstore.com";
//		String password = "Jarir123";

		String host = "jbemail01.jarirbookstore.com";
		String mailStoreType = "imaps";
		String username = "care-test@jarir.com";
		String password = "Jarir123";

		System.out.println("care-test@jarir.com");
		check(host, mailStoreType, username, password);

		// username = "care-test@jarirbookstore.com";
		// System.out.println("care-test@jarirbookstore.com");
		// check(host, mailStoreType, username, password);

	}

}